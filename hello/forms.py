from django import forms


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class LoginHeaderForm(forms.Form):
    user_name = forms.CharField(label='Username', max_length=200, initial='Email')