
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

$(document).ready(function () {
  $('#get-data').click(function () {
    var showData = $('#show-data');

    var formData = {"text": "kelly is awesome", "author": "Kelly"};

    $.ajax({
    url : "../create_post/",
    type: "POST",
    data : formData,
        "beforeSend": function(xhr, settings) {
        console.log("Before Send");
        $.ajaxSettings.beforeSend(xhr, settings);
    },
    success: function(data)
    {
        console.log(data);
        var content = '<br>' + data.key + '<br>' + data.value;
        showData.append(content);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        showData.text('Failed.');
    }
    });
  });
});


$('#get-data').click(function () {
  $.getJSON('example.json', function (data) {
    console.log(data);
  });
});