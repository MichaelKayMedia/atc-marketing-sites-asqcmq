# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-16 07:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hello', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitehtml',
            name='html',
            field=models.TextField(verbose_name='HTML'),
        ),
        migrations.AlterField(
            model_name='sitehtml',
            name='website',
            field=models.CharField(max_length=250, verbose_name='Site'),
        ),
    ]
