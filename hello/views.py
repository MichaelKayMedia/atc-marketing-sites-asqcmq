from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
import json
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.http import require_http_methods
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login

from django.contrib.auth.models import User, Group

from .models import ContactUsRequest, SiteHTML
from .forms import LoginHeaderForm

import collections

import squareconnect
import uuid
from squareconnect.rest import ApiException
from squareconnect.apis.checkout_api import CheckoutApi
from squareconnect.models import create_checkout_request, create_order_request, create_order_request_line_item, money

def pay_test(request):
	checkout_api = CheckoutApi()

	sandbox_access_key = 'sandbox-sq0atb-JqdrB-_CTP4ood_Z8s9iRQ'
	access_key = 'sq0atp-wY1q2KtajbyGWdAHC1iQCw'

	# access_key = sandbox_access_key
	checkout_api.api_client.configuration.access_token = access_key

	asq_cqi_catalog_id = 'WU2HZAVDFFMI6PV7YC5IJ6M5'
	asq_cre_catalog_id = '4JMWMWRPGDMNRSFLVDPK5IS3'
	atc_location_id = '7JB3NHJ0C2MWE'
	err_msg = ''
	link = ''
	link2 = ''
	try:


		guid = str(uuid.uuid4())

		checkout = create_checkout_request.CreateCheckoutRequest(guid)
		line_item = create_order_request_line_item.CreateOrderRequestLineItem(None, '1', None, None, None,
																			  asq_cqi_catalog_id, None, None, None)

		checkout.order = create_order_request.CreateOrderRequest(guid, None, [line_item], None, None)
		checkout_response = checkout_api.create_checkout(atc_location_id, checkout)

		link = checkout_response.checkout.checkout_page_url

		guid2 = str(uuid.uuid4())

		checkout2 = create_checkout_request.CreateCheckoutRequest(guid2)
		line_item2 = create_order_request_line_item.CreateOrderRequestLineItem(None, '1', None, None, None,
																			  asq_cre_catalog_id, None, None, None)

		checkout2.order = create_order_request.CreateOrderRequest(guid2, None, [line_item2], None, None)
		checkout_response2 = checkout_api.create_checkout(atc_location_id, checkout2)

		link2 = checkout_response2.checkout.checkout_page_url

	except ApiException as e:
		err_msg = e

	return render(request, 'pay_test.html', { "err" : '', "locations" : '', "link" : link, "link2" : link2 })


def pay_redirect(request, site_code):
	checkout_api = CheckoutApi()

	sandbox_access_key = 'sandbox-sq0atb-JqdrB-_CTP4ood_Z8s9iRQ'
	access_key = 'sq0atp-wY1q2KtajbyGWdAHC1iQCw'

	# access_key = sandbox_access_key
	checkout_api.api_client.configuration.access_token = access_key

	atc_location_id = '7JB3NHJ0C2MWE'
	err_msg = ''
	link = ''
	try:
		guid = str(uuid.uuid4())

		catalog_codes = SiteHTML.objects.all().filter(website=site_code)
		catalog_id = ''

		for x in catalog_codes:
			catalog_id = x.square_catalog_code

		return_url = 'http://www.asqcqe.com/thanks/'
		checkout = create_checkout_request.CreateCheckoutRequest(guid, redirect_url=return_url, ask_for_shipping_address=True)
		line_item = create_order_request_line_item.CreateOrderRequestLineItem(None, '1', None, None, None,
																			  catalog_id, None, None, None)

		if site_code == 'test':
			checkout.order = create_order_request.CreateOrderRequest(guid, None, [line_item], None, None)
		elif site_code == 'atc':
			price = request.GET['p']
			name = request.GET['n']

			if price != '':
				if name != '':
					m = money.Money(int(price) * 100, 'USD')
					li2 = create_order_request_line_item.CreateOrderRequestLineItem(name, '1', m, None, None,
																					  None, None, None, None)
					checkout.order = create_order_request.CreateOrderRequest(guid, None, [li2], None, None)
		else:
			checkout.order = create_order_request.CreateOrderRequest(guid, None, [line_item], None, None)

		checkout_response = checkout_api.create_checkout(atc_location_id, checkout)

		link = checkout_response.checkout.checkout_page_url

	except ApiException as e:
		err_msg = e

	if link == '':
		return redirect('/')

	return redirect(link)


def thanks(request):

	return render(request, 'order_thank_you.html', { })


class Link(object):
	def __init__(self, title, url):
		self.title = title
		self.url = url


def test_html(request):
	return render(request, 'test.html', {})

class faq:
	def __init__(self, title, faq_id, description, bullets, link):
		self.title = title
		self.id = faq_id
		self.description = description
		self.bullets = bullets
		self.link = link


def db_source(request):

	site = 'TEST'

	if "cmq" in request.META["HTTP_HOST"].lower():
		site = 'cmq'

	if "cqi" in request.META["HTTP_HOST"].lower():
		site = 'cqi'

	if "cba" in request.META["HTTP_HOST"].lower():
		site = 'cba'

	if "cqpa" in request.META["HTTP_HOST"].lower():
		site = 'cqpa'

	if "cqa" in request.META["HTTP_HOST"].lower():
		site = 'cqa'

	if "cqe" in request.META["HTTP_HOST"].lower():
		site = 'cqe'

	if "ccqm" in request.META["HTTP_HOST"].lower():
		site = 'ccqm'

	if "cct" in request.META["HTTP_HOST"].lower():
		site = 'cct'

	if "cqia" in request.META["HTTP_HOST"].lower():
		site = 'cqia'

	if "cqt" in request.META["HTTP_HOST"].lower():
		site = 'cqt'

	if "cre" in request.META["HTTP_HOST"].lower():
		site = 'cre'

	if "csqp" in request.META["HTTP_HOST"].lower():
		site = 'csqp'

	if "cssbb" in request.META["HTTP_HOST"].lower():
		site = 'cssbb'

	if "cssgb" in request.META["HTTP_HOST"].lower():
		site = 'cssgb'

	if "cssyb" in request.META["HTTP_HOST"].lower():
		site = 'cssyb'

	if "gsip" in request.META["HTTP_HOST"].lower():
		site = 'gsip'
		
	if "risingaboveitall" in request.META["HTTP_HOST"].lower():
		site = 'risingaboveitall'

	if "csqe" in request.META["HTTP_HOST"].lower():
		site = 'csqe'

	if "cmda" in request.META["HTTP_HOST"].lower():
		site = 'cmda'

	if site == "":
		site = request.META["HTTP_HOST"].lower()

	data = SiteHTML.objects.get(website=site)
	return render(request, 'db_source.html', {"html": data.html })


def index(request):
	# return HttpResponse('Hello from Python!')
	date = datetime.now().year


	faqs = [
		faq(''
			, 1, ''
			, [
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				"""""",
				""""""
			], ''),
		faq(''
			, 2, ''
			, [], ''),
		faq(''
			, 3, ''
			, [], ''),
		faq(''
			, 4, ''
			, [], ''),
		faq(''
			, 5, ''
			, [], ''),
		faq(''
			, 6, ''
			, [], ''),
		faq(''
			, 7, ''
			, [], ''),
		faq(''
			, 8, ''
			, [], ''),
		faq(''
			, 9, ''
			, [], ''),
		faq(''
			, 10, ''
			, [], ''),
		faq(''
			, 11, ''
			, [], ''),
		faq(''
			, 12, ''
			, [], ''),
		faq(''
			, 13, ''
			, [], ''),
		faq(''
			, 14, ''
			, [], ''),
		faq(''
			, 15, ''
			, [], ''),
		faq(''
			, 16, ''
			, [], ''),
		faq(''
			, 17, ''
			, [], ''),
		faq(''
			, 18, ''
			, [], ''),
		faq(''
			, 19, ''
			, [], ''),
		faq(''
			, 20, ''
			, [], '')
	]

	cssyb_faqs = [
		faq('Why take the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Yellow Belt experience.""",
				"""Your instructor has been preparing students for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) preparation efforts.  """,
				"""The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) prep class consists of 68 lectures that covers the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) prep class uses approximately 600 test question associated with the lectures and practice exams.  """,
				"""Part of our ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test preparation uses the Indiana Quality Councils CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam. """,
				""" After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% performance rate, or above, on your practice exams you are ready for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam. """,
				"""Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.  We guarantee you will pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) material in a natural format, similar to how you work through a Six Sigma Yellow Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Yellow Belt that will allow you to advance your career. """,
				"""We use three publications to help you prepare for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  They include the online publications (lectures and exams), CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.  We guarantee you will pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) examination is a one-part, 85 – multiple choice questions, three-and-a-half-hour exam and is offered in English only.  75 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 75- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 5, """Computer Delivered – The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) examination is a one-part, 85 – multiple choice questions, two-and-a-half-hour exam and is offered in English only.  75 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 75- multiple choice questions, your exam is offered in English only.  
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?  '
			, 6, '2 ½ hours for the electronic exam and 2 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, """I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, """Apply for membership in ASQ here.  https://asq.org/membership/become-a-member """
			, [], ''),
		faq('Where do I start my application for becoming a CSSYB (CERTIFIED SIX SIGMA YELLOW BELT)?'
			, 10, 'Apply for your CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-yellow-belt '
			, [], 'https://asq.org/cert/six-sigma-yellow-belt'),
		faq('How much does the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam cost?  '
			, 11, 'The cost of taking the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100. '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.'
			, [], ''),
		faq('Should I start my CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) application before starting my class?'
			, 13, """I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  """
			, [], ''),
		faq('What does the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSYB.'
			, [], ''),
		faq('What score is needed to pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification? '
			, 16, 'The CSSYB (Certified Six Sigma Yellow Belt) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification? '
			, 18, 'No experience required for the ASQ CSSYB (Certified Six Sigma Yellow Belt) certification exam.'
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	cssyb_faqs2 = [
		faq(
			'Why take the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Yellow Belt experience.""",
				"""Your instructor has been preparing students for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) preparation efforts.  """,
				"""The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) prep class consists of 68 lectures that covers the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) prep class uses approximately 600 test question associated with the lectures and practice exams.  """,
				"""Part of our CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test preparation uses the Indiana Quality Councils CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam. """,
				""" After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% performance rate, or above, on your practice exams you are ready for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam. """,
				"""Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam, and an optional first-time passing guarantee for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.  We guarantee you will pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) material in a natural format, similar to how you work through a Six Sigma Yellow Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Yellow Belt that will allow you to advance your career. """,
				"""We use three publications to help you prepare for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  They include the online publications (lectures and exams), CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam, and an optional first-time passing guarantee for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.  We guarantee you will pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) examination is a one-part, 85 – multiple choice questions, three-and-a-half-hour exam and is offered in English only.  75 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 75- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 5, """Computer Delivered – The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) examination is a one-part, 85 – multiple choice questions, two-and-a-half-hour exam and is offered in English only.  75 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 75- multiple choice questions, your exam is offered in English only.  
	"""
			, [], ''),
		faq('How much time do I have to complete the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?  '
			, 6,
			'2 ½ hours for the electronic exam and 2 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			"""I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, """Apply for membership in ASQ here.  https://asq.org/membership/become-a-member """
			, [], ''),
		faq('Where do I start my application for becoming a CSSYB (CERTIFIED SIX SIGMA YELLOW BELT)?'
			, 10,
			'Apply for your CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-yellow-belt '
			, [], 'https://asq.org/cert/six-sigma-yellow-belt'),
		faq('How much does the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam cost?  '
			, 11,
			'The cost of taking the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100. '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam.'
			, [], ''),
		faq('Should I start my CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) application before starting my class?'
			, 13,
			"""I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  """
			, [], ''),
		faq('What does the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSYB.'
			, [], ''),
		faq('What score is needed to pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification? '
			, 16,
			'The CSSYB (Certified Six Sigma Yellow Belt) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification? '
			, 18, 'No experience required for the CSSYB (Certified Six Sigma Yellow Belt) certification exam.'
			, [], ''),
		faq('What kind of a calculator can I bring into the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			"""Upon completion of the computer-based examination, you will receive a printed copy of your CSSYB (CERTIFIED SIX SIGMA YELLOW BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]
	csgb_faqs = [
		faq('Why take the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Green Belt experience.""",
				"""Your instructor has been preparing students for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) preparation efforts. """,
				"""The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) prep class consists of approximately 115 lectures that covers the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSGB (CERTIFIED SIX SIGMA GREEN BELT) prep class uses approximately 1,100 test question associated with the lectures and practice exams.  """,
				"""Part of our ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test preparation uses the Indiana Quality Councils CSSGB (CERTIFIED SIX SIGMA GREEN BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% performance rate, or above, on your practice exams you are ready for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam. """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  We guarantee you will pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam. """,
				"""We present the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) material in a natural format, similar to how you work through a Six Sigma Green Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Green Belt that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  They include the online publications (lectures and exams), CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends. """
			], ''),
		faq('Am I guaranteed to pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  We guarantee you will pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  '
			, [], ''),
		faq('What is your pass rate for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 5, """Computer Delivered – The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $288 and the cost as a non-member is $438 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CSSGB (CERTIFIED SIX SIGMA GREEN BELT)?'
			, 10, 'Apply for your CSSGB (CERTIFIED SIX SIGMA GREEN BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-green-belt '
			, [], 'https://asq.org/cert/six-sigma-green-belt'),
		faq('How much does the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam cost?  '
			, 11, 'The cost of taking the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam as an ASQ member is $288 and the cost as a non-member is $438 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100. '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  '
			, [], ''),
		faq('Should I start my CSSGB (CERTIFIED SIX SIGMA GREEN BELT) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSGB.'
			, [], ''),
		faq('What score is needed to pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSGB (CERTIFIED SIX SIGMA GREEN BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification?'
			, 16, 'The CSSGB (Certified Six Sigma Green Belt) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification? '
			, 18, """Six Sigma Green Belts are employees who spend some of their time on process improvement teams. They analyze and solve quality problems, and are involved with Six Sigma, lean or other quality improvement projects.
The Six Sigma Green Belt certification requires three years of work experience in one or more areas of the Six Sigma Green Belt Body of Knowledge.
Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, 'Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]

	csgb_faqs2 = [
		faq(
			'Why take the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Green Belt experience.""",
				"""Your instructor has been preparing students for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CSSGB (CERTIFIED SIX SIGMA GREEN BELT) preparation efforts. """,
				"""The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) prep class consists of approximately 115 lectures that covers the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSGB (CERTIFIED SIX SIGMA GREEN BELT) prep class uses approximately 1,100 test question associated with the lectures and practice exams.  """,
				"""Part of our CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test preparation uses the Indiana Quality Councils CSSGB (CERTIFIED SIX SIGMA GREEN BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% performance rate, or above, on your practice exams you are ready for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam. """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam, and an optional first-time passing guarantee for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  We guarantee you will pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam. """,
				"""We present the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) material in a natural format, similar to how you work through a Six Sigma Green Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Green Belt that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  They include the online publications (lectures and exams), CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends. """
			], ''),
		faq('Am I guaranteed to pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam, and an optional first-time passing guarantee for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  We guarantee you will pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  '
			, [], ''),
		faq('What is your pass rate for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 5, """Computer Delivered – The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.
	"""
			, [], ''),
		faq('How much time do I have to complete the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $288 and the cost as a non-member is $438 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CSSGB (CERTIFIED SIX SIGMA GREEN BELT)?'
			, 10,
			'Apply for your CSSGB (CERTIFIED SIX SIGMA GREEN BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-green-belt '
			, [], 'https://asq.org/cert/six-sigma-green-belt'),
		faq('How much does the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam cost?  '
			, 11,
			'The cost of taking the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam as an ASQ member is $288 and the cost as a non-member is $438 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100. '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam.  '
			, [], ''),
		faq('Should I start my CSSGB (CERTIFIED SIX SIGMA GREEN BELT) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSGB.'
			, [], ''),
		faq('What score is needed to pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSGB (CERTIFIED SIX SIGMA GREEN BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification?'
			, 16,
			'The CSSGB (Certified Six Sigma Green Belt) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CSSGB (CERTIFIED SIX SIGMA GREEN BELT) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification? '
			, 18, """Six Sigma Green Belts are employees who spend some of their time on process improvement teams. They analyze and solve quality problems, and are involved with Six Sigma, lean or other quality improvement projects.
	The Six Sigma Green Belt certification requires three years of work experience in one or more areas of the Six Sigma Green Belt Body of Knowledge.
	Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the CSSGB (CERTIFIED SIX SIGMA GREEN BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			'Upon completion of the computer-based examination, you will receive a printed copy of your CSSGB (CERTIFIED SIX SIGMA GREEN BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]

	cssbb_faqs = [
		faq('Why take the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Black Belt experience.""",
				"""Your instructor has been preparing students for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have developed specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) preparation efforts. """,
				"""The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class consists of approximately 150 lectures that covers the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class uses approximately 1,500 test question associated with the lectures and over practice exams.  """,
				"""Part of our ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test preparation uses the Indiana Quality Councils CSSBB (CERTIFIED SIX SIGMA BLACK BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% success rate, or above, on your practice exams you are ready for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 65%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  We guarantee you will pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) material in a natural format, similar to how you work through a Six Sigma Black Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Black Belt that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  They include the online publications (lectures and exams), CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.  """
			], ''),
		faq('Am I guaranteed to pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam, and an optional first-time passing guarantee for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  We guarantee you will pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.  The general population pass rate is approximately 65%.  '
			, [], ''),
		faq('What can you tell me about the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 5, """Computer Delivered – The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CSSBB (CERTIFIED SIX SIGMA BLACK BELT)?'
			, 10, 'Apply for your CSSBB (CERTIFIED SIX SIGMA BLACK BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-black-belt'
			, [], 'https://asq.org/cert/six-sigma-black-belt'),
		faq('How much does the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam cost?  '
			, 11, 'The cost of taking the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  '
			, [], ''),
		faq('Should I start my CSSBB (CERTIFIED SIX SIGMA BLACK BELT) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSBB.'
			, [], ''),
		faq('What score is needed to pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSBB (CERTIFIED SIX SIGMA BLACK BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification?'
			, 16, 'To maintain the integrity of your Six Sigma Black Belt certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period. '
			, [], ''),
		faq('Why should I become ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification? '
			, 18, """Six Sigma Black Belt requires two completed projects with signed affidavits or one completed project with signed affidavit and three years of work experience in one or more areas of the Six Sigma Body of Knowledge. For more information, please see the list of Six Sigma Project Affidavit FAQs. You do not need to be a Certified Six Sigma Green Belt.
Work experience must be in a full time, paid role. Paid intern, co-op or any other course work cannot be applied towards the work experience requirement.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on CSSBB in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	cssbb_faqs2 = [
		faq(
			'Why take the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Six Sigma Black Belt experience.""",
				"""Your instructor has been preparing students for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have developed specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CSSBB (CERTIFIED SIX SIGMA BLACK BELT) preparation efforts. """,
				"""The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class consists of approximately 150 lectures that covers the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) BOK in an easy to understand and enjoyable format.  """,
				"""Our CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class uses approximately 1,500 test question associated with the lectures and over practice exams.  """,
				"""Part of our CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test preparation uses the Indiana Quality Councils CSSBB (CERTIFIED SIX SIGMA BLACK BELT) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% success rate, or above, on your practice exams you are ready for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  This is what we have learned from 20 years of experience in preparing students for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 65%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam, and an optional first-time passing guarantee for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  We guarantee you will pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) material in a natural format, similar to how you work through a Six Sigma Black Belt project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Six Sigma Black Belt that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  They include the online publications (lectures and exams), CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.  """
			], ''),
		faq('Am I guaranteed to pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam, and an optional first-time passing guarantee for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  We guarantee you will pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.  The general population pass rate is approximately 65%.  '
			, [], ''),
		faq('What can you tell me about the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 5, """Computer Delivered – The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  
	"""
			, [], ''),
		faq('How much time do I have to complete the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CSSBB (CERTIFIED SIX SIGMA BLACK BELT)?'
			, 10,
			'Apply for your CSSBB (CERTIFIED SIX SIGMA BLACK BELT) application directly on ASQ’s website here.  https://asq.org/cert/six-sigma-black-belt'
			, [], 'https://asq.org/cert/six-sigma-black-belt'),
		faq('How much does the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam cost?  '
			, 11,
			'The cost of taking the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam.  '
			, [], ''),
		faq('Should I start my CSSBB (CERTIFIED SIX SIGMA BLACK BELT) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSSBB.'
			, [], ''),
		faq('What score is needed to pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSSBB (CERTIFIED SIX SIGMA BLACK BELT) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification?'
			, 16,
			'To maintain the integrity of your Six Sigma Black Belt certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period. '
			, [], ''),
		faq('Why should I become CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CSSBB (CERTIFIED SIX SIGMA BLACK BELT) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification? '
			, 18, """Six Sigma Black Belt requires two completed projects with signed affidavits or one completed project with signed affidavit and three years of work experience in one or more areas of the Six Sigma Body of Knowledge. For more information, please see the list of Six Sigma Project Affidavit FAQs. You do not need to be a Certified Six Sigma Green Belt.
	Work experience must be in a full time, paid role. Paid intern, co-op or any other course work cannot be applied towards the work experience requirement.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the CSSBB (CERTIFIED SIX SIGMA BLACK BELT) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on CSSBB in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			"""Upon completion of the computer-based examination, you will receive a printed copy of your CSSBB (CERTIFIED SIX SIGMA BLACK BELT) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	csqp_faqs = [
		faq('Why take the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Supplier Quality Professional experience.""",
				"""Your instructor has been preparing students for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) preparation efforts.  """,
				"""The ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) prep class consists of approximately 100 lectures that covers the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) BOK in an easy to understand and enjoyable format. """,
				"""Our ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) preparation class uses approximately 1,000 test questions associated with the lectures and practice exams. """,
				"""Part of our ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test preparation uses the Indiana Quality Councils ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) primer and the CSQP Handbook.  After every chapter of the Primer you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half these questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam. """,
				"""Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam, and an optional first-time passing guarantee for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.  We guarantee you will pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) material in a natural format, similar to how you work through a Supplier Quality Professional project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Supplier Quality Professional that will allow you to advance your career.""",
				"""We use three publications to help you prepare for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  They include the online publications (lectures and exams), ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam, and an optional first-time passing guarantee for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.  We guarantee you will pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 5, """Computer Delivered – The ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) examination is a one-part, 165 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL)?'
			, 10, 'Apply for your ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) application directly on ASQ’s website here.  https://asq.org/cert/supplier-quality '
			, [], 'https://asq.org/cert/supplier-quality'),
		faq('How much does the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam cost?'
			, 11, 'The cost of taking the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.'
			, [], ''),
		faq('Should I start my ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class. '
			, [], ''),
		faq('What does the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSQP.'
			, [], ''),
		faq('What score is needed to pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam? '
			, 15, """The pass percentage varies from one exam to another.  ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification? '
			, 16, 'The ASQ CSQP (Certified Supplier Quality Professional) is a lifetime certification.  No recertification is needed. '
			, [], ''),
		faq('Why should I become an ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification? '
			, 18, """Work experience must be in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Supplier Quality Professional Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
Candidates who were previously certified by ASQ as a quality engineer, quality auditor, software quality engineer, or quality manager, experience used to qualify for certification in these fields often applies to certification as a Supplier Quality Professional.
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the eight-year experience requirement waived, as follows (only one of these waivers may be claimed):
Diploma from a technical or trade school — one year will be waived
Associate degree — two years waived
Bachelor’s degree — four years waived
Master’s or doctorate — five years waived
Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	csqp_faqs2 = [
		faq(
			'Why take the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Supplier Quality Professional experience.""",
				"""Your instructor has been preparing students for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) preparation efforts.  """,
				"""The CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) prep class consists of approximately 100 lectures that covers the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) BOK in an easy to understand and enjoyable format. """,
				"""Our CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) preparation class uses approximately 1,000 test questions associated with the lectures and practice exams. """,
				"""Part of our CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test preparation uses the Indiana Quality Councils CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) primer and the CSQP Handbook.  After every chapter of the Primer you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half these questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  This is what we have learned from 20 years of experience in preparing students for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam. """,
				"""Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam, and an optional first-time passing guarantee for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.  We guarantee you will pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  """,
				"""We present the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) material in a natural format, similar to how you work through a Supplier Quality Professional project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Supplier Quality Professional that will allow you to advance your career.""",
				"""We use three publications to help you prepare for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  They include the online publications (lectures and exams), CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam, and an optional first-time passing guarantee for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.  We guarantee you will pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 5, """Computer Delivered – The CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) examination is a one-part, 165 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.
	"""
			, [], ''),
		faq('How much time do I have to complete the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL)?'
			, 10,
			'Apply for your CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) application directly on ASQ’s website here.  https://asq.org/cert/supplier-quality '
			, [], 'https://asq.org/cert/supplier-quality'),
		faq('How much does the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam cost?'
			, 11,
			'The cost of taking the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam.'
			, [], ''),
		faq('Should I start my CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class. '
			, [], ''),
		faq('What does the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CSQP.'
			, [], ''),
		faq('What score is needed to pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam? '
			, 15, """The pass percentage varies from one exam to another.  CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification? '
			, 16,
			'The CSQP (Certified Supplier Quality Professional) is a lifetime certification.  No recertification is needed. '
			, [], ''),
		faq('Why should I become an CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification? '
			, 18, """Work experience must be in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Supplier Quality Professional Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
	Candidates who were previously certified by ASQ as a quality engineer, quality auditor, software quality engineer, or quality manager, experience used to qualify for certification in these fields often applies to certification as a Supplier Quality Professional.
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the eight-year experience requirement waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical or trade school — one year will be waived
	Associate degree — two years waived
	Bachelor’s degree — four years waived
	Master’s or doctorate — five years waived
	Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			"""Upon completion of the computer-based examination, you will receive a printed copy of your CSQP (CERTIFIED SUPPLIER QUALITY PROFESSIONAL) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	cre_faqs = [
		faq('Why take the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Reliability Engineer experience.""",
				"""Your instructor has been preparing students for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CRE (CERTIFIED RELIABILITY ENGINEER) preparation efforts.  """,
				"""The CRE (CERTIFIED RELIABILITY ENGINEER) prep class consists of 70 lectures that covers the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) BOK in an easy to understand and enjoyable format. """,
				"""Our CRE (CERTIFIED RELIABILITY ENGINEER) prep class uses approximately 700 test question associated with the lectures and our practice exams.  """,
				"""Part of our ASQ CRE (CERTIFIED RELIABILITY ENGINEER) test preparation uses the Indiana Quality Councils CRE (CERTIFIED RELIABILITY ENGINEER) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exam. Once you experience an 80% performance rate, or above, on your practice exam you are ready for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam.""",
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 50%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam, and an optional first-time passing guarantee for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam.  We guarantee you will pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """,
				"""We present the CRE (CERTIFIED RELIABILITY ENGINEER) material in a natural format, similar to how you work through a Reliability Engineer project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Reliability Engineer that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  They include the online publications (lectures and exams), CRE (CERTIFIED RELIABILITY ENGINEER) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.   """
			], ''),
		faq('Am I guaranteed to pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam, and an optional first-time passing guarantee for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam.  We guarantee you will pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  '
			, [], ''),
		faq('What is your pass rate for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   The pass rate for the general population is approximately 50%. '
			, [], ''),
		faq('What can you tell me about the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CRE (CERTIFIED RELIABILITY ENGINEER) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 5, """Computer Delivered – The CRE (CERTIFIED RELIABILITY ENGINEER) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CRE (CERTIFIED RELIABILITY ENGINEER)?'
			, 10, 'Apply for your CRE (CERTIFIED RELIABILITY ENGINEER) application directly on ASQ’s website here.  https://asq.org/cert/reliability-engineer '
			, [], 'https://asq.org/cert/reliability-engineer'),
		faq('How much does the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam cost?  '
			, 11, 'The cost of taking the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam.  '
			, [], ''),
		faq('Should I start my CRE (CERTIFIED RELIABILITY ENGINEER) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CRE (CERTIFIED RELIABILITY ENGINEER) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CRE.'
			, [], ''),
		faq('What score is needed to pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam? '
			, 15, """The pass percentage varies from one exam to another.  CRE (CERTIFIED RELIABILITY ENGINEER) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CRE (CERTIFIED RELIABILITY ENGINEER) certification?'
			, 16, 'To maintain the integrity of your Reliability Engineer certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CRE (CERTIFIED RELIABILITY ENGINEER) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CRE (CERTIFIED RELIABILITY ENGINEER) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Reliability Engineer Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
Candidates who were previously certified by ASQ as a quality engineer, quality auditor, software quality engineer, or quality manager, experience used to qualify for certification in these fields often applies to certification as a reliability engineer.
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the eight-year experience requirement waived, as follows (only one of these waivers may be claimed):
Diploma from a technical or trade school — one year will be waived
Associate degree — two years waived
Bachelor’s degree — four years waived
Master’s or doctorate — five years waived
Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, 'Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CRE (CERTIFIED RELIABILITY ENGINEER) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]

	cre_faqs2 = [
		faq(
			'Why take the  CRE (CERTIFIED RELIABILITY ENGINEER) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred  CRE (CERTIFIED RELIABILITY ENGINEER) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Reliability Engineer experience.""",
				"""Your instructor has been preparing students for the  CRE (CERTIFIED RELIABILITY ENGINEER) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!). """,
				"""All of our lectures in the  CRE (CERTIFIED RELIABILITY ENGINEER) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CRE (CERTIFIED RELIABILITY ENGINEER) preparation efforts.  """,
				"""The CRE (CERTIFIED RELIABILITY ENGINEER) prep class consists of 70 lectures that covers the  CRE (CERTIFIED RELIABILITY ENGINEER) BOK in an easy to understand and enjoyable format. """,
				"""Our CRE (CERTIFIED RELIABILITY ENGINEER) prep class uses approximately 700 test question associated with the lectures and our practice exams.  """,
				"""Part of our  CRE (CERTIFIED RELIABILITY ENGINEER) test preparation uses the Indiana Quality Councils CRE (CERTIFIED RELIABILITY ENGINEER) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exam. Once you experience an 80% performance rate, or above, on your practice exam you are ready for the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  This is what we have learned from 20 years of experience in preparing students for the  CRE (CERTIFIED RELIABILITY ENGINEER) exam.""",
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 50%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam, and an optional first-time passing guarantee for the  CRE (CERTIFIED RELIABILITY ENGINEER) exam.  We guarantee you will pass the  CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """,
				"""We present the CRE (CERTIFIED RELIABILITY ENGINEER) material in a natural format, similar to how you work through a Reliability Engineer project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Reliability Engineer that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  They include the online publications (lectures and exams), CRE (CERTIFIED RELIABILITY ENGINEER) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CRE (CERTIFIED RELIABILITY ENGINEER) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.   """
			], ''),
		faq('Am I guaranteed to pass the  CRE (CERTIFIED RELIABILITY ENGINEER) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam, and an optional first-time passing guarantee for the  CRE (CERTIFIED RELIABILITY ENGINEER) exam.  We guarantee you will pass the  CRE (CERTIFIED RELIABILITY ENGINEER) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.  '
			, [], ''),
		faq('What is your pass rate for the  CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   The pass rate for the general population is approximately 50%. '
			, [], ''),
		faq('What can you tell me about the  CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CRE (CERTIFIED RELIABILITY ENGINEER) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the  CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 5, """Computer Delivered – The CRE (CERTIFIED RELIABILITY ENGINEER) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  
	"""
			, [], ''),
		faq('How much time do I have to complete the  CRE (CERTIFIED RELIABILITY ENGINEER) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the  CRE (CERTIFIED RELIABILITY ENGINEER) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CRE (CERTIFIED RELIABILITY ENGINEER)?'
			, 10,
			'Apply for your CRE (CERTIFIED RELIABILITY ENGINEER) application directly on ASQ’s website here.  https://asq.org/cert/reliability-engineer '
			, [], 'https://asq.org/cert/reliability-engineer'),
		faq('How much does the  CRE (CERTIFIED RELIABILITY ENGINEER) exam cost?  '
			, 11,
			'The cost of taking the  CRE (CERTIFIED RELIABILITY ENGINEER) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CRE (CERTIFIED RELIABILITY ENGINEER) exam.  '
			, [], ''),
		faq('Should I start my CRE (CERTIFIED RELIABILITY ENGINEER) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CRE (CERTIFIED RELIABILITY ENGINEER) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CRE.'
			, [], ''),
		faq('What score is needed to pass the  CRE (CERTIFIED RELIABILITY ENGINEER) exam? '
			, 15, """The pass percentage varies from one exam to another.  CRE (CERTIFIED RELIABILITY ENGINEER) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CRE (CERTIFIED RELIABILITY ENGINEER) certification?'
			, 16,
			'To maintain the integrity of your Reliability Engineer certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become  CRE (CERTIFIED RELIABILITY ENGINEER) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CRE (CERTIFIED RELIABILITY ENGINEER) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CRE (CERTIFIED RELIABILITY ENGINEER) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the  CRE (CERTIFIED RELIABILITY ENGINEER) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Reliability Engineer Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
	Candidates who were previously certified by ASQ as a quality engineer, quality auditor, software quality engineer, or quality manager, experience used to qualify for certification in these fields often applies to certification as a reliability engineer.
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the eight-year experience requirement waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical or trade school — one year will be waived
	Associate degree — two years waived
	Bachelor’s degree — four years waived
	Master’s or doctorate — five years waived
	Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the  CRE (CERTIFIED RELIABILITY ENGINEER) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CRE (CERTIFIED RELIABILITY ENGINEER) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			'Upon completion of the computer-based examination, you will receive a printed copy of your  CRE (CERTIFIED RELIABILITY ENGINEER) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]


	cqt_faqs = [
		faq('Why take the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Quality Technician experience.""",
				"""Your instructor has been preparing students for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.""",
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CQT (CERTIFIED QUALITY TECHNICIAN) preparation efforts.  """,
				"""The CQT (CERTIFIED QUALITY TECHNICIAN) prep class consists of 83 lectures that covers the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) BOK in an easy to understand and enjoyable format.  """,
				"""Our CQT (CERTIFIED QUALITY TECHNICIAN) prep class uses approximately 900 test questions associated with the lectures and our practice exams.  """,
				"""Part of our ASQ CQT (CERTIFIED QUALITY TECHNICIAN) test preparation uses the Indiana Quality Councils CQT (CERTIFIED QUALITY TECHNICIAN) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam.  """,
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 50%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam, and an optional first-time passing guarantee for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam.  We guarantee you will pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam. """,
				"""We present the CQT (CERTIFIED QUALITY TECHNICIAN) material in a natural format, similar to how you work through a Quality Technician project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Technician that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  They include the online publications (lectures and exams), CQT (CERTIFIED QUALITY TECHNICIAN) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam?'
			, 2, """Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam, and an optional first-time passing guarantee for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam.  We guarantee you will pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """
			, [], ''),
		faq('What is your pass rate for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass. The pass rate for students that do not participate in our program is approximately a 50% first time pass rate.   '
			, [], ''),
		faq('What can you tell me about the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CQT (CERTIFIED QUALITY TECHNICIAN) examination is a one-part, 110 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 5, """Computer Delivered – The CQT (CERTIFIED QUALITY TECHNICIAN) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.  
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.'
			, [], ''),
		faq('Where can I take the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, """I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CQT (CERTIFIED QUALITY TECHNICIAN)?'
			, 10, 'Apply for your CQT (CERTIFIED QUALITY TECHNICIAN) application directly on ASQ’s website here.  https://asq.org/cert/quality-technician'
			, [], 'https://asq.org/cert/quality-technician'),
		faq('How much does the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam cost?  '
			, 11, 'The cost of taking the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam.  '
			, [], ''),
		faq('Should I start my CQT (CERTIFIED QUALITY TECHNICIAN) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CQT (CERTIFIED QUALITY TECHNICIAN) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CQT.'
			, [], ''),
		faq('What score is needed to pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam? '
			, 15, """The pass percentage varies from one exam to another.  CQT (CERTIFIED QUALITY TECHNICIAN) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CQT (CERTIFIED QUALITY TECHNICIAN) certification?'
			, 16, 'The CQT (Certified Quality Technician) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CQT (CERTIFIED QUALITY TECHNICIAN) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQT (CERTIFIED QUALITY TECHNICIAN) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have at least four years of higher education and/or work experience in one or more of the areas of the Certified Quality Technician Body of Knowledge.
Candidates who were previously certified by ASQ as a quality engineer, quality auditor, reliability engineer, software quality engineer or quality manager, experience used to qualify for certification in those fields applies to certification as a quality technician.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQT (CERTIFIED QUALITY TECHNICIAN) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	cqt_faqs2 = [
		faq(
			'Why take the  CQT (CERTIFIED QUALITY TECHNICIAN) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred  CQT (CERTIFIED QUALITY TECHNICIAN) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Quality Technician experience.""",
				"""Your instructor has been preparing students for the  CQT (CERTIFIED QUALITY TECHNICIAN) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.""",
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the  CQT (CERTIFIED QUALITY TECHNICIAN) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CQT (CERTIFIED QUALITY TECHNICIAN) preparation efforts.  """,
				"""The CQT (CERTIFIED QUALITY TECHNICIAN) prep class consists of 83 lectures that covers the  CQT (CERTIFIED QUALITY TECHNICIAN) BOK in an easy to understand and enjoyable format.  """,
				"""Our CQT (CERTIFIED QUALITY TECHNICIAN) prep class uses approximately 900 test questions associated with the lectures and our practice exams.  """,
				"""Part of our  CQT (CERTIFIED QUALITY TECHNICIAN) test preparation uses the Indiana Quality Councils CQT (CERTIFIED QUALITY TECHNICIAN) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  This is what we have learned from 20 years of experience in preparing students for the  CQT (CERTIFIED QUALITY TECHNICIAN) exam.  """,
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 50%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam, and an optional first-time passing guarantee for the  CQT (CERTIFIED QUALITY TECHNICIAN) exam.  We guarantee you will pass the  CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam. """,
				"""We present the CQT (CERTIFIED QUALITY TECHNICIAN) material in a natural format, similar to how you work through a Quality Technician project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Technician that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  They include the online publications (lectures and exams), CQT (CERTIFIED QUALITY TECHNICIAN) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CQT (CERTIFIED QUALITY TECHNICIAN) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the  CQT (CERTIFIED QUALITY TECHNICIAN) exam?'
			, 2,
			"""Alpha Superior Quality Training has an industry leading pass rate for the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam, and an optional first-time passing guarantee for the  CQT (CERTIFIED QUALITY TECHNICIAN) exam.  We guarantee you will pass the  CQT (CERTIFIED QUALITY TECHNICIAN) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """
			, [], ''),
		faq('What is your pass rate for the  CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass. The pass rate for students that do not participate in our program is approximately a 50% first time pass rate.   '
			, [], ''),
		faq('What can you tell me about the  CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CQT (CERTIFIED QUALITY TECHNICIAN) examination is a one-part, 110 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the  CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 5, """Computer Delivered – The CQT (CERTIFIED QUALITY TECHNICIAN) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.  
	"""
			, [], ''),
		faq('How much time do I have to complete the  CQT (CERTIFIED QUALITY TECHNICIAN) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.'
			, [], ''),
		faq('Where can I take the  CQT (CERTIFIED QUALITY TECHNICIAN) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			"""I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CQT (CERTIFIED QUALITY TECHNICIAN)?'
			, 10,
			'Apply for your CQT (CERTIFIED QUALITY TECHNICIAN) application directly on ASQ’s website here.  https://asq.org/cert/quality-technician'
			, [], 'https://asq.org/cert/quality-technician'),
		faq('How much does the  CQT (CERTIFIED QUALITY TECHNICIAN) exam cost?  '
			, 11,
			'The cost of taking the  CQT (CERTIFIED QUALITY TECHNICIAN) exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CQT (CERTIFIED QUALITY TECHNICIAN) exam.  '
			, [], ''),
		faq('Should I start my CQT (CERTIFIED QUALITY TECHNICIAN) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CQT (CERTIFIED QUALITY TECHNICIAN) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CQT.'
			, [], ''),
		faq('What score is needed to pass the  CQT (CERTIFIED QUALITY TECHNICIAN) exam? '
			, 15, """The pass percentage varies from one exam to another.  CQT (CERTIFIED QUALITY TECHNICIAN) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CQT (CERTIFIED QUALITY TECHNICIAN) certification?'
			, 16, 'The CQT (Certified Quality Technician) is a lifetime certification.  No recertification is needed.'
			, [], ''),
		faq('Why should I become  CQT (CERTIFIED QUALITY TECHNICIAN) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CQT (CERTIFIED QUALITY TECHNICIAN) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CQT (CERTIFIED QUALITY TECHNICIAN) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the  CQT (CERTIFIED QUALITY TECHNICIAN) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have at least four years of higher education and/or work experience in one or more of the areas of the Certified Quality Technician Body of Knowledge.
	Candidates who were previously certified by ASQ as a quality engineer, quality auditor, reliability engineer, software quality engineer or quality manager, experience used to qualify for certification in those fields applies to certification as a quality technician.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the  CQT (CERTIFIED QUALITY TECHNICIAN) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CQT (CERTIFIED QUALITY TECHNICIAN) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			"""Upon completion of the computer-based examination, you will receive a printed copy of your  CQT (CERTIFIED QUALITY TECHNICIAN) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates."""
			, [], '')
	]

	cqia_faqs = [
		faq('Why take the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Quality Improvement Associate experience.""",
				"""Your instructor has been preparing students for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.""",
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) preparation efforts.  """,
				"""The CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class consists of 47 lectures that covers the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) BOK in an easy to understand and enjoyable format.  """,
				"""Our CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class uses approximately 400 test question associated with the lectures and over practice exams.  """,
				"""Part of our ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test preparation uses the Indiana Quality Councils CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  """,
				"""	Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam, and an optional first-time passing guarantee for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  We guarantee you will pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """,
				"""We present the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) material in a natural format, similar to how you work through a Quality Improvement Associate project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Improvement Associate that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  They include the online publications (lectures and exams), CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam, and an optional first-time passing guarantee for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  We guarantee you will pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.
Computer Delivered - The CQIA examination is a one-part, 110-question, three-and-a-half-hour exam and is offered in English only. 100 questions are scored and 10 are unscored.
Paper and Pencil - The CQIA examination is a one-part, 100-question, three-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.
"""
			, [], ''),
		faq('How many questions are on the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 5, """Computer Delivered – The CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) examination is a one-part, 110 – multiple choice questions, three-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?  '
			, 6, '3 ½ hours for the electronic exam and 3 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.'
			, [], ''),
		faq('Where can I take the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, """I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?'
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member'
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE)?'
			, 10, 'Apply for your CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) application directly on ASQ’s website here.  https://asq.org/cert/quality-improvement-associate'
			, [], 'https://asq.org/cert/quality-improvement-associate'),
		faq('How much does the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam cost?  '
			, 11, 'The cost of taking the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.'
			, [], ''),
		faq('Should I start my CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) application before starting my class?'
			, 13, """I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  """
			, [], ''),
		faq('What does the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CQIA.'
			, [], ''),
		faq('What score is needed to pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam? '
			, 15, """The pass percentage varies from one exam to another.  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification? '
			, 16, 'The CQIA (Certified Quality Improvement Associate) is a lifetime certification.  No recertification is needed. '
			, [], ''),
		faq('Why should I become ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification? '
			, 18, 'No experience required for the ASQ CQIA (Certified Quality Improvement Associate) certification exam.'
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """
Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
"""
			, [], '')
	]

	cqia_faqs2 = [
		faq(
			'Why take the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Quality Improvement Associate experience.""",
				"""Your instructor has been preparing students for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.""",
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) preparation efforts.  """,
				"""The CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class consists of 47 lectures that covers the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) BOK in an easy to understand and enjoyable format.  """,
				"""Our CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class uses approximately 400 test question associated with the lectures and over practice exams.  """,
				"""Part of our  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test preparation uses the Indiana Quality Councils CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  This is what we have learned from 20 years of experience in preparing students for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  """,
				"""	Our first time pass rate is over 95% compared to the general populations pass rate of approximately 80%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""Alpha Superior Quality Training has an industry leading pass rate for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam, and an optional first-time passing guarantee for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  We guarantee you will pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   """,
				"""We present the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) material in a natural format, similar to how you work through a Quality Improvement Associate project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Improvement Associate that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  They include the online publications (lectures and exams), CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam, and an optional first-time passing guarantee for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.  We guarantee you will pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.
	Computer Delivered - The CQIA examination is a one-part, 110-question, three-and-a-half-hour exam and is offered in English only. 100 questions are scored and 10 are unscored.
	Paper and Pencil - The CQIA examination is a one-part, 100-question, three-hour exam and is offered in English only.
	All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.
	"""
			, [], ''),
		faq('How many questions are on the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 5, """Computer Delivered – The CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) examination is a one-part, 110 – multiple choice questions, three-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.
	"""
			, [], ''),
		faq('How much time do I have to complete the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?  '
			, 6,
			'3 ½ hours for the electronic exam and 3 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.'
			, [], ''),
		faq('Where can I take the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			"""I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  """
			, [], ''),
		faq('Where do I start my application for membership in ASQ?'
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member'
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE)?'
			, 10,
			'Apply for your CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) application directly on ASQ’s website here.  https://asq.org/cert/quality-improvement-associate'
			, [], 'https://asq.org/cert/quality-improvement-associate'),
		faq('How much does the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam cost?  '
			, 11,
			'The cost of taking the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam as an ASQ member is $244 and the cost as a non-member is $394 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam.'
			, [], ''),
		faq('Should I start my CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) application before starting my class?'
			, 13,
			"""I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  """
			, [], ''),
		faq('What does the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CQIA.'
			, [], ''),
		faq('What score is needed to pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam? '
			, 15, """The pass percentage varies from one exam to another.  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification? '
			, 16,
			'The CQIA (Certified Quality Improvement Associate) is a lifetime certification.  No recertification is needed. '
			, [], ''),
		faq('Why should I become  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification? '
			, 18, 'No experience required for the  CQIA (Certified Quality Improvement Associate) certification exam.'
			, [], ''),
		faq('What kind of a calculator can I bring into the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, """
	Upon completion of the computer-based examination, you will receive a printed copy of your  CQIA (CERTIFIED QUALITY IMPROVEMENT ASSOCIATE) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
	"""
			, [], '')
	]

	cct_faqs = [
		faq('Why take the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Calibration Technician experience.""",
				"""Your instructor has been preparing students for the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).""",
				"""All of our lectures in the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) preparation efforts.  """,
				"""The CCT (CERTIFIED CALIBRATION TECHNICIAN) prep class consists of 55 lectures that covers the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) BOK in an easy to understand and enjoyable format.  """,
				"""Our CCT (CERTIFIED CALIBRATION TECHNICIAN) preparation class uses approximately 500 test question associated with the lectures and practice exams.  """,
				"""Part of our ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) test preparation uses the Indiana Quality Councils CCT (CERTIFIED CALIBRATION TECHNICIAN) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  """,
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 70%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""We have the best guarantee in the industry.  We guarantee you will pass the ASQ CCT (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   """,
				"""We present the CCT (CERTIFIED CALIBRATION TECHNICIAN) material in a natural format, similar to how you work through a Calibration Technician project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Calibration Technician that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam.  They include the online publications (lectures and exams), CCT (CERTIFIED CALIBRATION TECHNICIAN) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?'
			, 2, """Alpha Superior Quality Training has an industry leading pass rate for the ASQ CCT (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the ASQ CCT (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the ASQ CCT (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam."""
			, [], ''),
		faq('What is your pass rate for the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.'
			, [], ''),
		faq('What can you tell me about the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CCT (CERTIFIED CALIBRATION TECHNICIAN) examination is a one-part, 135 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, for your exam and is offered in English only. This is a 4 hour exam.    
All examinations are open book.  Each participant must bring his or her own reference materials.
"""
			, [], ''),
		faq('How many questions are on the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 5, """Computer Delivered – The CCT (CERTIFIED CALIBRATION TECHNICIAN) examination is a one-part, 135 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, five your exam and is offered in English only. 
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?  '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CCT (CERTIFIED CALIBRATION TECHNICIAN)?'
			, 10, 'Apply for your CCT (CERTIFIED CALIBRATION TECHNICIAN) application directly on ASQ’s website here.  https://asq.org/cert/calibration-technician'
			, [], ''),
		faq('How much does the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam cost?  '
			, 11, 'The cost of taking the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam.  '
			, [], ''),
		faq('Should I start my CCT (CERTIFIED CALIBRATION TECHNICIAN) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class. '
			, [], ''),
		faq('What does the CCT (CERTIFIED CALIBRATION TECHNICIAN) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CCT.'
			, [], ''),
		faq('What score is needed to pass the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam? '
			, 15, """The pass percentage varies from one exam to another.  CCT (CERTIFIED CALIBRATION TECHNICIAN) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CCT (CERTIFIED CALIBRATION TECHNICIAN) certification?'
			, 16, 'To maintain the integrity of your Calibration Technician certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CCT (CERTIFIED CALIBRATION TECHNICIAN) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) cert is highly respected within industry. 
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) certification? '
			, 18, """ Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the five-year experience requirement waived, as follows (only one of these waivers may be claimed):
Diploma from a technical, military, or trade school — two years waived
Associate degree — two year waived
Bachelor's degree — two years waived
Master's or doctorate — two years waived
Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?'
			, 20, 'Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CCT (CERTIFIED CALIBRATION TECHNICIAN) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]

	cct_faqs2 = [
		faq(
			'Why take the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Calibration Technician experience.""",
				"""Your instructor has been preparing students for the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).""",
				"""All of our lectures in the  CCT (CERTIFIED CALIBRATION TECHNICIAN) test prep class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CCT (CERTIFIED CALIBRATION TECHNICIAN) preparation efforts.  """,
				"""The CCT (CERTIFIED CALIBRATION TECHNICIAN) prep class consists of 55 lectures that covers the  CCT (CERTIFIED CALIBRATION TECHNICIAN) BOK in an easy to understand and enjoyable format.  """,
				"""Our CCT (CERTIFIED CALIBRATION TECHNICIAN) preparation class uses approximately 500 test question associated with the lectures and practice exams.  """,
				"""Part of our  CCT (CERTIFIED CALIBRATION TECHNICIAN) test preparation uses the Indiana Quality Councils CCT (CERTIFIED CALIBRATION TECHNICIAN) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  """,
				"""Our first time pass rate is over 85% compared to the general populations pass rate of approximately 70%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""We have the best guarantee in the industry.  We guarantee you will pass the  CCT (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   """,
				"""We present the CCT (CERTIFIED CALIBRATION TECHNICIAN) material in a natural format, similar to how you work through a Calibration Technician project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Calibration Technician that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the  CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam.  They include the online publications (lectures and exams), CCT (CERTIFIED CALIBRATION TECHNICIAN) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CCT (CERTIFIED CALIBRATION TECHNICIAN) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    """
			], ''),
		faq('Am I guaranteed to pass the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?'
			, 2,
			"""Alpha Superior Quality Training has an industry leading pass rate for the  CCT (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the  CCT (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the  CCT (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam."""
			, [], ''),
		faq('What is your pass rate for the  CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 85% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.'
			, [], ''),
		faq('What can you tell me about the  CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CCT (CERTIFIED CALIBRATION TECHNICIAN) examination is a one-part, 135 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, for your exam and is offered in English only. This is a 4 hour exam.    
	All examinations are open book.  Each participant must bring his or her own reference materials.
	"""
			, [], ''),
		faq('How many questions are on the  CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 5, """Computer Delivered – The CCT (CERTIFIED CALIBRATION TECHNICIAN) examination is a one-part, 135 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, five your exam and is offered in English only. 
	"""
			, [], ''),
		faq('How much time do I have to complete the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?  '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the  CCT (CERTIFIED CALIBRATION TECHNICIAN) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member'),
		faq('Where do I start my application for becoming a CCT (CERTIFIED CALIBRATION TECHNICIAN)?'
			, 10,
			'Apply for your CCT (CERTIFIED CALIBRATION TECHNICIAN) application directly on ASQ’s website here.  https://asq.org/cert/calibration-technician'
			, [], ''),
		faq('How much does the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam cost?  '
			, 11,
			'The cost of taking the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam as an ASQ member is $268 and the cost as a non-member is $418 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.'
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam.  '
			, [], ''),
		faq('Should I start my CCT (CERTIFIED CALIBRATION TECHNICIAN) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class. '
			, [], ''),
		faq('What does the CCT (CERTIFIED CALIBRATION TECHNICIAN) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CCT.'
			, [], ''),
		faq('What score is needed to pass the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam? '
			, 15, """The pass percentage varies from one exam to another.  CCT (CERTIFIED CALIBRATION TECHNICIAN) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CCT (CERTIFIED CALIBRATION TECHNICIAN) certification?'
			, 16,
			'To maintain the integrity of your Calibration Technician certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become  CCT (CERTIFIED CALIBRATION TECHNICIAN) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CCT (CERTIFIED CALIBRATION TECHNICIAN) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CCT (CERTIFIED CALIBRATION TECHNICIAN) cert is highly respected within industry. 
	"""
			, [], ''),
		faq('What are the work requirements for the  CCT (CERTIFIED CALIBRATION TECHNICIAN) certification? '
			, 18, """ Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the five-year experience requirement waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical, military, or trade school — two years waived
	Associate degree — two year waived
	Bachelor's degree — two years waived
	Master's or doctorate — two years waived
	Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the  CCT (CERTIFIED CALIBRATION TECHNICIAN) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CCT (CERTIFIED CALIBRATION TECHNICIAN) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?'
			, 20,
			'Upon completion of the computer-based examination, you will receive a printed copy of your  CCT (CERTIFIED CALIBRATION TECHNICIAN) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]



	ccqm_faqs = [
		faq('Why take the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam preparation course from Alpha Superior Quality Training?'
			, 1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Construction Quality Manager experience.""",
				"""Your instructor has been preparing students for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test preparation class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) preparation efforts.  """,
				"""The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class consists of 103 lectures that covers the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) BOK in an easy to understand and enjoyable format.  """,
				"""Our CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class uses approximately 1,000 test question associated with the lectures and over practice exams.  """,
				"""Part of our ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test preparation uses the Indiana Quality Councils CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam.  """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""We have the best guarantee in the industry.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   """,
				"""We present the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) material in a natural format, similar to how you work through a Construction Quality Manager project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Construction Quality Manager that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  They include the online publications (lectures and exams), CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends."""
			], ''),
		faq('Am I guaranteed to pass the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam?'
			, 2, 'Alpha Superior Quality Training has an industry leading pass rate for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 
"""
			, [], ''),
		faq('How many questions are on the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 5, """Computer Delivered – The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, this exam is offered in English only.  
"""
			, [], ''),
		faq('How much time do I have to complete the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam? '
			, 6, '4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member '),
		faq('Where do I start my application for becoming a CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER)?'
			, 10, 'Apply for your CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) application directly on ASQ’s website here.  https://asq.org/cert/construction-quality '
			, [], 'https://asq.org/cert/construction-quality'),
		faq('How much does the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam cost?  '
			, 11, 'The cost of taking the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam.  '
			, [], ''),
		faq('Should I start my CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) application before starting my class?'
			, 13, 'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CCQM.'
			, [], ''),
		faq('What score is needed to pass the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam? '
			, 15, """The pass percentage varies from one exam to another.  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
"""
			, [], ''),
		faq('What is needed to maintain a CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification? '
			, 16, 'To maintain the integrity of your Construction Quality Manager certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) cert is highly respected within industry.  
"""
			, [], ''),
		faq('What are the work requirements for the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Construction Quality Manager Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
For candidates who were certified by ASQ as a quality engineer, Construction Quality Manager, supplier quality professional, software quality engineer or quality manager, the experience used to qualify for certification in these fields applies to certification as a Construction Quality Manager.      
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ, part of the eight-year experience requirement will be waived, as follows (only one of these waivers may be claimed):
Diploma from a technical or trade school — one year will be waived
Associate degree — two years waived
Bachelor’s degree — four years waived
Master’s or doctorate — five years waived
Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
"""
			, [], ''),
		faq('What kind of a calculator can I bring into the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20, 'Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]

	cqa_faqs = [
		faq(
			'Why take the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam preparation course from Alpha Superior Quality Training?',
			1, """
				We know you have a choice.  Our goal is to be the most preferred ASQ CQA (CERTIFIED QUALITY AUDITOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
				""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality Auditor experience.
				""",
				"""
				Your instructor has been preparing students for the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the ASQ CQA (CERTIFIED QUALITY AUDITOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CQA (CERTIFIED QUALITY AUDITOR) preparation efforts.  
				""",
				"""
				The CQA (CERTIFIED QUALITY AUDITOR) prep class consists of 79 lectures that covers the ASQ CQA (CERTIFIED QUALITY AUDITOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CQA (CERTIFIED QUALITY AUDITOR) prep class uses approximately 800 test question associated with the lectures and our practice exams.
				""",
				"""
				Part of our ASQ CQA (CERTIFIED QUALITY AUDITOR) test preparation uses the Indiana Quality Councils CQA (CERTIFIED QUALITY AUDITOR) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam.  This is what we have learned from 20 years of experience in preparing students for the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam, and an optional first-time passing guarantee for the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam.  We guarantee you will pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   
				""",
				"""
				We present the CQA (CERTIFIED QUALITY AUDITOR) material in a natural format, similar to how you work through a Quality Auditor project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Auditor that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam.  They include the online publications (lectures and exams), CQA (CERTIFIED QUALITY AUDITOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost. 
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CQA (CERTIFIED QUALITY AUDITOR) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam?',
			2,
			'18.	Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam, and an optional first-time passing guarantee for the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam.  We guarantee you will pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the ASQ CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  The general ASQ CQA pass rate is approximately 75%. In summary what we have found is those students who follow our plan will pass.   ',
			[], None),
		faq('What can you tell me about the ASQ CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			4,
			"""
Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CQA (CERTIFIED QUALITY AUDITOR) examination is a one-part, 165 – multiple choice question, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 


			""",
			[], None),
		faq('How many questions are on the ASQ CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			5, """
Computer Delivered – The CQA (CERTIFIED QUALITY AUDITOR) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  


				""", [], None),
		faq(
			'How much time do I have to complete the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam?',
			6, """
				5 ½ hours for the electronic exam and 5 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.    
				""", [], None),
		faq('Where can I take the ASQ CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
				I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CQA (CERTIFIED QUALITY AUDITOR)?',
			10,
			'Apply for your CQA (CERTIFIED QUALITY AUDITOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/quality-auditor')),
		faq('How much does the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam cost?',
			11, """

The cost of taking the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  

				""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
				$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam.  
				""", [], None),
		faq(
			'Should I start my CQA (CERTIFIED QUALITY AUDITOR) application before starting my class?',
			13, """
				I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
				""", [], None),
		faq(
			'What does the CQA (CERTIFIED QUALITY AUDITOR) Body of Knowledge consist of?',
			14,
			'See link on home page for ASQ BOK for the CQA. ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/docs/2012%20CQA%20Final%20BOK.pdf')),
		faq(
			'What score is needed to pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam?',
			15,
			"""

The pass percentage varies from one exam to another.  CQA (CERTIFIED QUALITY AUDITOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CQA (CERTIFIED QUALITY AUDITOR) certification? ',
			16, """
				To maintain the integrity of your Quality Auditor certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  
				""", [], None),
		faq('Why should I become ASQ CQA (CERTIFIED QUALITY AUDITOR) certified?',
			17, """
Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CQA (CERTIFIED QUALITY AUDITOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQA (CERTIFIED QUALITY AUDITOR) cert is highly respected within industry.  

				""",
			[], None),
		faq(
			'What are the work requirements for the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification?',
			18, """

Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Quality Auditor Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
For candidates who were certified by ASQ as a quality engineer, reliability engineer, supplier quality professional, software quality engineer or quality manager, the experience used to qualify for certification in these fields applies to certification as a quality auditor.
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ, part of the eight-year experience requirement will be waived, as follows (only one of these waivers may be claimed):
Diploma from a technical or trade school — one year will be waived
Associate degree — two years waived
Bachelor’s degree — four years waived
Master’s or doctorate — five years waived
Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.


				""", [], None),
		faq(
			'What kind of a calculator can I bring into the ASQ CQA (CERTIFIED QUALITY AUDITOR) exam?',
			19,
			"""
I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQA (CERTIFIED QUALITY AUDITOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?',
			20, """

				Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQA (CERTIFIED QUALITY AUDITOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
				""", [], None),
	]

	ccqm_faqs2 = [
		faq(
			'Why take the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam preparation course from Alpha Superior Quality Training?'
			, 1,
			'We know you have a choice.  Our goal is to be the most preferred  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam preparation vendor in the world.  Our program is different and better than others and this is why:'
			, [
				"""You instructor is friendly, educated, and has real- world Construction Quality Manager experience.""",
				"""Your instructor has been preparing students for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam for over 20 years.""",
				"""Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!""",
				"""Your instructor has won the prestigious Shingo Award for research and publication.  """,
				"""From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  """,
				"""All of our lectures in the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test preparation class are in HD video and may be watched over your mobile devices.""",
				"""Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) preparation efforts.  """,
				"""The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class consists of 103 lectures that covers the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) BOK in an easy to understand and enjoyable format.  """,
				"""Our CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class uses approximately 1,000 test question associated with the lectures and over practice exams.  """,
				"""Part of our  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test preparation uses the Indiana Quality Councils CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam. """,
				"""After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  This is what we have learned from 20 years of experience in preparing students for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam.  """,
				"""Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.""",
				"""We have the best guarantee in the industry.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   """,
				"""We present the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) material in a natural format, similar to how you work through a Construction Quality Manager project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Construction Quality Manager that will allow you to advance your career.  """,
				"""We use three publications to help you prepare for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  They include the online publications (lectures and exams), CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. """,
				"""You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.""",
				"""You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends."""
			], ''),
		faq('Am I guaranteed to pass the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam?'
			, 2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   '
			, [], ''),
		faq('What is your pass rate for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   '
			, [], ''),
		faq('What can you tell me about the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 4, """Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) examination is a one-part, 165 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 
	"""
			, [], ''),
		faq('How many questions are on the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 5, """Computer Delivered – The CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, this exam is offered in English only.  
	"""
			, [], ''),
		faq('How much time do I have to complete the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam? '
			, 6,
			'4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  '
			, [], ''),
		faq('Where can I take the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Exam?'
			, 7,
			'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a PROMETRIC site near you go to https://securereg3.prometric.com/Dispatch.aspx .'
			, [], 'https://securereg3.prometric.com/Dispatch.aspx'),
		faq('Should I become a member of ASQ?'
			, 8,
			'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('Where do I start my application for membership in ASQ?  '
			, 9, 'Apply for membership in ASQ here.  https://asq.org/membership/become-a-member '
			, [], 'https://asq.org/membership/become-a-member '),
		faq('Where do I start my application for becoming a CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER)?'
			, 10,
			'Apply for your CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) application directly on ASQ’s website here.  https://asq.org/cert/construction-quality '
			, [], 'https://asq.org/cert/construction-quality'),
		faq('How much does the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam cost?  '
			, 11,
			'The cost of taking the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  '
			, [], ''),
		faq('How much does membership in ASQ cost?'
			, 12,
			'$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam.  '
			, [], ''),
		faq('Should I start my CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) application before starting my class?'
			, 13,
			'I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  '
			, [], ''),
		faq('What does the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) Body of Knowledge consist of?  '
			, 14, 'See link on home page for ASQ BOK for the CCQM.'
			, [], ''),
		faq('What score is needed to pass the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam? '
			, 15, """The pass percentage varies from one exam to another.  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
	Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
	 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.
	"""
			, [], ''),
		faq('What is needed to maintain a CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification? '
			, 16,
			'To maintain the integrity of your Construction Quality Manager certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  '
			, [], ''),
		faq('Why should I become  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certified?'
			, 17, """Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) cert is highly respected within industry.  
	"""
			, [], ''),
		faq('What are the work requirements for the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification? '
			, 18, """Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Construction Quality Manager Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
	For candidates who were certified by ASQ as a quality engineer, Construction Quality Manager, supplier quality professional, software quality engineer or quality manager, the experience used to qualify for certification in these fields applies to certification as a Construction Quality Manager.      
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ, part of the eight-year experience requirement will be waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical or trade school — one year will be waived
	Associate degree — two years waived
	Bachelor’s degree — four years waived
	Master’s or doctorate — five years waived
	Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.
	"""
			, [], ''),
		faq('What kind of a calculator can I bring into the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) exam?'
			, 19, """I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
	All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
	With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
	Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.
	"""
			, [], ''),
		faq('How long does it take to get my exam results?  '
			, 20,
			'Upon completion of the computer-based examination, you will receive a printed copy of your  CCQM (CERTIFIED CONSTRUCTION QUALITY MANAGER) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.'
			, [], '')
	]



	cqa_faqs2 = [
		faq(
			'Why take the CQA (CERTIFIED QUALITY AUDITOR) exam preparation course from Alpha Superior Quality Training?',
			1, """
					We know you have a choice.  Our goal is to be the most preferred CQA (CERTIFIED QUALITY AUDITOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
					""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality Auditor experience.
				""",
				"""
				Your instructor has been preparing students for the CQA (CERTIFIED QUALITY AUDITOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the CQA (CERTIFIED QUALITY AUDITOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CQA (CERTIFIED QUALITY AUDITOR) preparation efforts.  
				""",
				"""
				The CQA (CERTIFIED QUALITY AUDITOR) prep class consists of 79 lectures that covers the CQA (CERTIFIED QUALITY AUDITOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CQA (CERTIFIED QUALITY AUDITOR) prep class uses approximately 800 test question associated with the lectures and our practice exams.
				""",
				"""
				Part of our CQA (CERTIFIED QUALITY AUDITOR) test preparation uses the Indiana Quality Councils CQA (CERTIFIED QUALITY AUDITOR) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters, as applicable.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CQA (CERTIFIED QUALITY AUDITOR) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams. Once you experience an 80% rate, or above, on your practice exams you are ready for the CQA (CERTIFIED QUALITY AUDITOR) certification exam.  This is what we have learned from 20 years of experience in preparing students for the CQA (CERTIFIED QUALITY AUDITOR) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 60%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				Alpha Superior Quality Training has an industry leading pass rate for the CQA (CERTIFIED QUALITY AUDITOR) certification exam, and an optional first-time passing guarantee for the CQA (CERTIFIED QUALITY AUDITOR) exam.  We guarantee you will pass the CQA (CERTIFIED QUALITY AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   
				""",
				"""
				We present the CQA (CERTIFIED QUALITY AUDITOR) material in a natural format, similar to how you work through a Quality Auditor project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CQA (CERTIFIED QUALITY AUDITOR) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Auditor that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the CQA (CERTIFIED QUALITY AUDITOR) certification exam.  They include the online publications (lectures and exams), CQA (CERTIFIED QUALITY AUDITOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost. 
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CQA (CERTIFIED QUALITY AUDITOR) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the CQA (CERTIFIED QUALITY AUDITOR) exam?',
			2,
			'18.	Alpha Superior Quality Training has an industry leading pass rate for the CQA (CERTIFIED QUALITY AUDITOR) certification exam, and an optional first-time passing guarantee for the CQA (CERTIFIED QUALITY AUDITOR) exam.  We guarantee you will pass the CQA (CERTIFIED QUALITY AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  The general CQA pass rate is approximately 75%. In summary what we have found is those students who follow our plan will pass.   ',
			[], None),
		faq('What can you tell me about the CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			4,
			"""
Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CQA (CERTIFIED QUALITY AUDITOR) examination is a one-part, 165 – multiple choice question, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 


			""",
			[], None),
		faq('How many questions are on the CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			5, """
	Computer Delivered – The CQA (CERTIFIED QUALITY AUDITOR) examination is a one-part, 165 – multiple choice questions, five-and-a-half-hour exam and is offered in English only.  150 multiple choice questions are scored and 15 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 150- multiple choice questions, five your exam and is offered in English only.  


					""", [], None),
		faq(
			'How much time do I have to complete the CQA (CERTIFIED QUALITY AUDITOR) exam?',
			6, """
					5 ½ hours for the electronic exam and 5 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.    
					""", [], None),
		faq('Where can I take the CQA (CERTIFIED QUALITY AUDITOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
					I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CQA (CERTIFIED QUALITY AUDITOR)?',
			10,
			'Apply for your CQA (CERTIFIED QUALITY AUDITOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/quality-auditor')),
		faq('How much does the CQA (CERTIFIED QUALITY AUDITOR) exam cost?',
			11, """

	The cost of taking the CQA (CERTIFIED QUALITY AUDITOR) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  

					""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
					$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CQA (CERTIFIED QUALITY AUDITOR) exam.  
					""", [], None),
		faq(
			'Should I start my CQA (CERTIFIED QUALITY AUDITOR) application before starting my class?',
			13, """
					I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
					""", [], None),
		faq(
			'What does the CQA (CERTIFIED QUALITY AUDITOR) Body of Knowledge consist of?',
			14,
			'See link on home page for ASQ BOK for the CQA. ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/docs/2012%20CQA%20Final%20BOK.pdf')),
		faq(
			'What score is needed to pass the CQA (CERTIFIED QUALITY AUDITOR) exam?',
			15,
			"""

The pass percentage varies from one exam to another.  CQA (CERTIFIED QUALITY AUDITOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CQA (CERTIFIED QUALITY AUDITOR) certification? ',
			16, """
					To maintain the integrity of your Quality Auditor certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.  
					""", [], None),
		faq('Why should I become CQA (CERTIFIED QUALITY AUDITOR) certified?',
			17, """
	Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CQA (CERTIFIED QUALITY AUDITOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CQA (CERTIFIED QUALITY AUDITOR) cert is highly respected within industry.  

					""",
			[], None),
		faq(
			'What are the work requirements for the CQA (CERTIFIED QUALITY AUDITOR) certification?',
			18, """

	Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have eight years of on-the-job experience in one or more of the areas of the Certified Quality Auditor Body of Knowledge. A minimum of three years of this experience must be in a decision-making position. "Decision-making" is defined as the authority to define, execute, or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
	For candidates who were certified by ASQ as a quality engineer, reliability engineer, supplier quality professional, software quality engineer or quality manager, the experience used to qualify for certification in these fields applies to certification as a quality auditor.
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ, part of the eight-year experience requirement will be waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical or trade school — one year will be waived
	Associate degree — two years waived
	Bachelor’s degree — four years waived
	Master’s or doctorate — five years waived
	Degrees/diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.


					""", [], None),
		faq(
			'What kind of a calculator can I bring into the CQA (CERTIFIED QUALITY AUDITOR) exam?',
			19,
			"""
I recommend the TI-30XA.  This calculator will offer everything you need to pass the CQA (CERTIFIED QUALITY AUDITOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?',
			20, """

					Upon completion of the computer-based examination, you will receive a printed copy of your CQA (CERTIFIED QUALITY AUDITOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
					""", [], None),
	]




	cqpa_faqs = [
		faq(
			'Why take the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam preparation course from Alpha Superior Quality Training?',
			1, """
				We know you have a choice.  Our goal is to be the most preferred ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam preparation vendor in the world.  Our program is different and better than others and this is why:
					""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) preparation efforts.  
				""",
				"""
				The ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST)  prep class consists of 111 lectures that covers the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST)  preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) test prep uses the Indiana Quality Councils ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification.  This is what we have learned from 20 years of experience in preparing students for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We present the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST)  material in a natural format, similar to how you work through a Manager of Quality / Operational Excellence project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST)  exam on the first try and the second objective is to educate you in such a way as to make you a powerful Manager of Quality Process Analyst that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam.  They include the online publications (lectures and exams), CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?',
			2,
			'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam, and an optional first-time passing guarantee for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  We guarantee you will pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass. ',
			[], None),
		faq('What can you tell me about the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			5, """
Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CQPA (CERTIFIED QUALITY PROCESS ANALYST) examination is a one-part, 110 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 

				""", [], None),
		faq(
			'How much time do I have to complete the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?  ',
			6, """
				4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
				""", [], None),
		faq('Where can I take the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
				I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST)?',
			10,
			'Apply for your ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/manager-of-quality')),
		faq('How much does the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam cost?  ',
			11, """

				The cost of taking the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
				$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  
				""", [], None),
		faq(
			'Should I start my ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) application before starting my class?',
			13, """
				I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
				""", [], None),
		faq(
			'What does the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) Body of Knowledge consist of?  ',
			14,
			'To see the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq(
			'What score is needed to pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam? ',
			15,
			"""
			The pass percentage varies from one exam to another.  ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification? ',
			16, """
				The CQPA (Certified Quality Process Analyst) is a lifetime certification.  No recertification is needed. 
				""", [], None),
		faq('Why should I become ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certified?',
			17, """
				Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the The CQPA (Certified Quality Process Analyst) is a lifetime certification.  No recertification is needed.  certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) cert is highly respected within industry.  

				""",
			[], None),
		faq(
			'What are the work requirements for the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification?  ',
			18, """

Candidates must have two years of work experience, or an associate degree or two years of equivalent higher education.
Work experience must relate to one or more areas of the CQPA Body of Knowledge.


				""", [], None),
		faq(
			'What kind of a calculator can I bring into the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

				Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
				""", [], None),
	]

	cqpa_faqs2 = [
		faq(
			'Why take the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam preparation course from Alpha Superior Quality Training?',
			1, """
					We know you have a choice.  Our goal is to be the most preferred CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam preparation vendor in the world.  Our program is different and better than others and this is why:
						""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the CQPA (CERTIFIED QUALITY PROCESS ANALYST) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CQPA (CERTIFIED QUALITY PROCESS ANALYST) preparation efforts.  
				""",
				"""
				The CQPA (CERTIFIED QUALITY PROCESS ANALYST)  prep class consists of 111 lectures that covers the CQPA (CERTIFIED QUALITY PROCESS ANALYST) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CQPA (CERTIFIED QUALITY PROCESS ANALYST)  preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our CQPA (CERTIFIED QUALITY PROCESS ANALYST) test prep uses the Indiana Quality Councils CQPA (CERTIFIED QUALITY PROCESS ANALYST) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification.  This is what we have learned from 20 years of experience in preparing students for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We present the CQPA (CERTIFIED QUALITY PROCESS ANALYST)  material in a natural format, similar to how you work through a Manager of Quality / Operational Excellence project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST)  exam on the first try and the second objective is to educate you in such a way as to make you a powerful Manager of Quality Process Analyst that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam.  They include the online publications (lectures and exams), CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CQPA (CERTIFIED QUALITY PROCESS ANALYST) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?',
			2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam, and an optional first-time passing guarantee for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  We guarantee you will pass the ASQ CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the certification exam.   ',
			[], None),
		faq('What is your pass rate for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 90% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass. ',
			[], None),
		faq('What can you tell me about the CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			5, """
	Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
	Computer Delivered – The CQPA (CERTIFIED QUALITY PROCESS ANALYST) examination is a one-part, 110 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, for your exam and is offered in English only.  
	All examinations are open book.  Each participant must bring his or her own reference materials. 

					""", [], None),
		faq(
			'How much time do I have to complete the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?  ',
			6, """
					4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
					""", [], None),
		faq('Where can I take the CQPA (CERTIFIED QUALITY PROCESS ANALYST) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
					I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CQPA (CERTIFIED QUALITY PROCESS ANALYST)?',
			10,
			'Apply for your CQPA (CERTIFIED QUALITY PROCESS ANALYST) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/manager-of-quality')),
		faq('How much does the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam cost?  ',
			11, """

					The cost of taking the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
					$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam.  
					""", [], None),
		faq(
			'Should I start my CQPA (CERTIFIED QUALITY PROCESS ANALYST) application before starting my class?',
			13, """
					I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
					""", [], None),
		faq(
			'What does the CQPA (CERTIFIED QUALITY PROCESS ANALYST) Body of Knowledge consist of?  ',
			14,
			'To see the CQPA (CERTIFIED QUALITY PROCESS ANALYST) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq(
			'What score is needed to pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam? ',
			15,
			"""
			The pass percentage varies from one exam to another.  CQPA (CERTIFIED QUALITY PROCESS ANALYST) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification? ',
			16, """
					The CQPA (Certified Quality Process Analyst) is a lifetime certification.  No recertification is needed. 
					""", [], None),
		faq('Why should I become CQPA (CERTIFIED QUALITY PROCESS ANALYST) certified?',
			17, """
					Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
		One of my students was recently looking for a new job without success.  This student decided to get the The CQPA (Certified Quality Process Analyst) is a lifetime certification.  No recertification is needed.  certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CQPA (CERTIFIED QUALITY PROCESS ANALYST) cert is highly respected within industry.  

					""",
			[], None),
		faq(
			'What are the work requirements for the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification?  ',
			18, """

	Candidates must have two years of work experience, or an associate degree or two years of equivalent higher education.
	Work experience must relate to one or more areas of the CQPA Body of Knowledge.


					""", [], None),
		faq(
			'What kind of a calculator can I bring into the CQPA (CERTIFIED QUALITY PROCESS ANALYST) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the CQPA (CERTIFIED QUALITY PROCESS ANALYST) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

					Upon completion of the computer-based examination, you will receive a printed copy of your CQPA (CERTIFIED QUALITY PROCESS ANALYST) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
					""", [], None),
	]

	cqi_faqs = [
		faq(
			'Why take the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam preparation course from Alpha Superior Quality Training?',
			1, """
				We know you have a choice.  Our goal is to be the most preferred ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
					""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the ASQ CQI (CERTIFIED QUALITY INSPECTOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CQI (CERTIFIED QUALITY INSPECTOR) preparation efforts.  
				""",
				"""
				The ASQ CQI (CERTIFIED QUALITY INSPECTOR) prep class consists of 51 lectures that covers the ASQ CQI (CERTIFIED QUALITY INSPECTOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our ASQ CQI (CERTIFIED QUALITY INSPECTOR) preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our ASQ CQI (CERTIFIED QUALITY INSPECTOR) test prep uses the Indiana Quality Councils CQI (CERTIFIED QUALITY INSPECTOR) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification.  This is what we have learned from 20 years of experience in preparing students for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification exam, and an optional first-time passing guarantee for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam.  We guarantee you will pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   
				""",
				"""
				We present the CQI (CERTIFIED QUALITY INSPECTOR) material in a natural format, similar to how you work through a Quality Inspector project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Inspector that will allow you to advance your career.  
				""",
				"""
				15.	We use three publications to help you prepare for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification exam.  They include the online publications (lectures and exams), CQI (CERTIFIED QUALITY INSPECTOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CQI (CERTIFIED QUALITY INSPECTOR) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam?',
			2,
			'18.	Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification exam, and an optional first-time passing guarantee for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam.  We guarantee you will pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.    ',
			[], None),
		faq('What can you tell me about the ASQ CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the ASQ CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			5, """
Computer Delivered – The CQI (CERTIFIED QUALITY INSPECTOR) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.  

				""", [], None),
		faq(
			'How much time do I have to complete the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam?  ',
			6, """
				4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
				""", [], None),
		faq('Where can I take the ASQ CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
				I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CQI (CERTIFIED QUALITY INSPECTOR)?',
			10,
			'Apply for your ASQ CQI (CERTIFIED QUALITY INSPECTOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/quality-inspector')),
		faq('How much does the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam cost?  ',
			11, """

				The cost of taking the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
				$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam.  
				""", [], None),
		faq(
			'Should I start my ASQ CQI (CERTIFIED QUALITY INSPECTOR) application before starting my class?',
			13, """
				I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
				""", [], None),
		faq(
			'What does the ASQ CQI (CERTIFIED QUALITY INSPECTOR) Body of Knowledge consist of?  ',
			14,
			'To see the ASQ CQI (CERTIFIED QUALITY INSPECTOR) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/certification/new-cqi-bok-2012.pdf')),
		faq(
			'What score is needed to pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam? ',
			15,
			"""
The pass percentage varies from one exam to another.  CQI (CERTIFIED QUALITY INSPECTOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification? ',
			16, """
				The CQI (Certified Quality Inspector) is a lifetime certification.  No recertification is needed.

 
Why should I become ASQ CQI (CERTIFIED QUALITY INSPECTOR) certified?
Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CQI (CERTIFIED QUALITY INSPECTOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQI (CERTIFIED QUALITY INSPECTOR) cert is highly respected within industry.  

				""", [], None),
		faq('Why should I become ASQ CQI (CERTIFIED QUALITY INSPECTOR) certified?',
			17, """
Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CQI (CERTIFIED QUALITY INSPECTOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CQI (CERTIFIED QUALITY INSPECTOR) cert is highly respected within industry.  

				""",
			[], None),
		faq(
			'What are the work requirements for the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification?  ',
			18, """

Work experience must be in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Two years of on-the-job experience in mechanical inspection or a related field.
A high-school diploma or GED or an additional three years of related on-the-job experience is necessary.
Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.

				""", [], None),
		faq(
			'What kind of a calculator can I bring into the ASQ CQI (CERTIFIED QUALITY INSPECTOR) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQI (CERTIFIED QUALITY INSPECTOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

				Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQI (CERTIFIED QUALITY INSPECTOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
				""", [], None),
	]

	cqi_faqs2 = [
		faq(
			'Why take the CQI (CERTIFIED QUALITY INSPECTOR) exam preparation course from Alpha Superior Quality Training?',
			1, """
					We know you have a choice.  Our goal is to be the most preferred CQI (CERTIFIED QUALITY INSPECTOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
						""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the CQI (CERTIFIED QUALITY INSPECTOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the CQI (CERTIFIED QUALITY INSPECTOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CQI (CERTIFIED QUALITY INSPECTOR) preparation efforts.  
				""",
				"""
				The CQI (CERTIFIED QUALITY INSPECTOR) prep class consists of 51 lectures that covers the CQI (CERTIFIED QUALITY INSPECTOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CQI (CERTIFIED QUALITY INSPECTOR) preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our CQI (CERTIFIED QUALITY INSPECTOR) test prep uses the Indiana Quality Councils CQI (CERTIFIED QUALITY INSPECTOR) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the CQI (CERTIFIED QUALITY INSPECTOR) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the CQI (CERTIFIED QUALITY INSPECTOR) certification.  This is what we have learned from 20 years of experience in preparing students for the CQI (CERTIFIED QUALITY INSPECTOR) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				Alpha Superior Quality Training has an industry leading pass rate for the CQI (CERTIFIED QUALITY INSPECTOR) certification exam, and an optional first-time passing guarantee for the CQI (CERTIFIED QUALITY INSPECTOR) exam.  We guarantee you will pass the CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   
				""",
				"""
				We present the CQI (CERTIFIED QUALITY INSPECTOR) material in a natural format, similar to how you work through a Quality Inspector project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Quality Inspector that will allow you to advance your career.  
				""",
				"""
				15.	We use three publications to help you prepare for the CQI (CERTIFIED QUALITY INSPECTOR) certification exam.  They include the online publications (lectures and exams), CQI (CERTIFIED QUALITY INSPECTOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts (this is not applicable for all certification classes).  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CQI (CERTIFIED QUALITY INSPECTOR) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the CQI (CERTIFIED QUALITY INSPECTOR) exam?',
			2,
			'18.	Alpha Superior Quality Training has an industry leading pass rate for the CQI (CERTIFIED QUALITY INSPECTOR) certification exam, and an optional first-time passing guarantee for the CQI (CERTIFIED QUALITY INSPECTOR) exam.  We guarantee you will pass the CQI (CERTIFIED QUALITY INSPECTOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.    ',
			[], None),
		faq('What can you tell me about the CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			5, """
	Computer Delivered – The CQI (CERTIFIED QUALITY INSPECTOR) examination is a one-part, 110 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  100 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 100- multiple choice questions, five your exam and is offered in English only.  

					""", [], None),
		faq(
			'How much time do I have to complete the CQI (CERTIFIED QUALITY INSPECTOR) exam?  ',
			6, """
					4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
					""", [], None),
		faq('Where can I take the CQI (CERTIFIED QUALITY INSPECTOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
					I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CQI (CERTIFIED QUALITY INSPECTOR)?',
			10,
			'Apply for your CQI (CERTIFIED QUALITY INSPECTOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/quality-inspector')),
		faq('How much does the CQI (CERTIFIED QUALITY INSPECTOR) exam cost?  ',
			11, """

					The cost of taking the CQI (CERTIFIED QUALITY INSPECTOR) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
					$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CQI (CERTIFIED QUALITY INSPECTOR) exam.  
					""", [], None),
		faq(
			'Should I start my CQI (CERTIFIED QUALITY INSPECTOR) application before starting my class?',
			13, """
					I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
					""", [], None),
		faq(
			'What does the CQI (CERTIFIED QUALITY INSPECTOR) Body of Knowledge consist of?  ',
			14,
			'To see the CQI (CERTIFIED QUALITY INSPECTOR) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/certification/new-cqi-bok-2012.pdf')),
		faq(
			'What score is needed to pass the CQI (CERTIFIED QUALITY INSPECTOR) exam? ',
			15,
			"""
The pass percentage varies from one exam to another.  CQI (CERTIFIED QUALITY INSPECTOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CQI (CERTIFIED QUALITY INSPECTOR) certification? ',
			16, """
					The CQI (Certified Quality Inspector) is a lifetime certification.  No recertification is needed.


	Why should I become CQI (CERTIFIED QUALITY INSPECTOR) certified?
	Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CQI (CERTIFIED QUALITY INSPECTOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CQI (CERTIFIED QUALITY INSPECTOR) cert is highly respected within industry.  

					""", [], None),
		faq('Why should I become CQI (CERTIFIED QUALITY INSPECTOR) certified?',
			17, """
	Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CQI (CERTIFIED QUALITY INSPECTOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CQI (CERTIFIED QUALITY INSPECTOR) cert is highly respected within industry.  

					""",
			[], None),
		faq(
			'What are the work requirements for the CQI (CERTIFIED QUALITY INSPECTOR) certification?  ',
			18, """

	Work experience must be in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Two years of on-the-job experience in mechanical inspection or a related field.
	A high-school diploma or GED or an additional three years of related on-the-job experience is necessary.
	Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.

					""", [], None),
		faq(
			'What kind of a calculator can I bring into the CQI (CERTIFIED QUALITY INSPECTOR) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the CQI (CERTIFIED QUALITY INSPECTOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

					Upon completion of the computer-based examination, you will receive a printed copy of your CQI (CERTIFIED QUALITY INSPECTOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
					""", [], None),
	]

	cba_faqs = [
		faq(
			'Why take the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam Preparation course from Alpha Superior Quality Training?',
			1, """
				We know you have a choice.  Our goal is to be the most preferred ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
				""",
			[
				"""
				You instructor is friendly, educated, and has real- world Biomedical Auditor experience.
				""",
				"""
				Your instructor has been preparing students for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) preparation efforts.  
				""",
				"""
				The ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) prep class consists of 85 lectures that covers the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) preparation class uses approximately 700 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) test prep uses the ASQ Biomedical Handbook (CERTIFIED BIOMEDICAL AUDITOR).  
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 1 practice exam available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification.  
				""",
				"""
				Our first time pass rate is over 85% compared to the general populations pass rate of approximately 65%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We use three publications to help you prepare for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam.  They include the online publications (lectures and exams), CBA (CERTIFIED BIOMEDICAL AUDITOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends
				""",

			], None),
		faq('Am I guaranteed to pass the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?',
			2,
			'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   ',
			[], None),
		faq('What can you tell me about the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			4,
			"""
Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CBA (CERTIFIED BIOMEDICAL AUDITOR) examination is a one-part, 145 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  135 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 135- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 

			""",
			[], None),
		faq('How many questions are on the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			5, """
Computer Delivered – The CBA (CERTIFIED BIOMEDICAL AUDITOR) examination is a one-part, 135 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, five your exam and is offered in English only.  

				""", [], None),
		faq(
			'How much time do I have to complete the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?    ',
			6, """
				4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
				""", [], None),
		faq('Where can I take the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
				I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR)?',
			10,
			'Apply for your ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/biomedical-auditor')),
		faq('How much does the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam cost?  ',
			11, """

				The cost of taking the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
				$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  
				""", [], None),
		faq(
			'Should I start my ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) application before starting my class?',
			13, """
				I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
				""", [], None),
		faq(
			'What does the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) Body of Knowledge consist of?  ',
			14,
			'To see the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq(
			'What score is needed to pass the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam? ',
			15,
			"""
The pass percentage varies from one exam to another.  CBA (CERTIFIED BIOMEDICAL AUDITOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification? ',
			16, """
				To maintain the integrity of your Biomedical Auditor certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.   
				""", [], None),
		faq('Why should I become ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certified?',
			17, """
Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) cert is highly respected within industry.  

				""",
			[], None),
		faq(
			'What are the work requirements for the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification?  ',
			18, """

 Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
The CBA requires five years of work experience including one year in a decision-making position.
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the five-year experience requirement waived, as follows (only one of these waivers may be claimed):
Associate degree — one year waived
Bachelor's degree — three years waived
Master's or doctorate — four years waived
Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.     


				""", [], None),
		faq(
			'What kind of a calculator can I bring into the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

				Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CBA (CERTIFIED BIOMEDICAL AUDITOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
				""", [], None),
	]

	cba_faqs2 = [
		faq(
			'Why take the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam Preparation course from Alpha Superior Quality Training?',
			1, """
					We know you have a choice.  Our goal is to be the most preferred CBA (CERTIFIED BIOMEDICAL AUDITOR) exam preparation vendor in the world.  Our program is different and better than others and this is why:
					""",
			[
				"""
				You instructor is friendly, educated, and has real- world Biomedical Auditor experience.
				""",
				"""
				Your instructor has been preparing students for the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the CBA (CERTIFIED BIOMEDICAL AUDITOR) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful CBA (CERTIFIED BIOMEDICAL AUDITOR) preparation efforts.  
				""",
				"""
				The CBA (CERTIFIED BIOMEDICAL AUDITOR) prep class consists of 85 lectures that covers the CBA (CERTIFIED BIOMEDICAL AUDITOR) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CBA (CERTIFIED BIOMEDICAL AUDITOR) preparation class uses approximately 700 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our CBA (CERTIFIED BIOMEDICAL AUDITOR) test prep uses the ASQ Biomedical Handbook (CERTIFIED BIOMEDICAL AUDITOR).  
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 1 practice exam available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification.  
				""",
				"""
				Our first time pass rate is over 85% compared to the general populations pass rate of approximately 65%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We use three publications to help you prepare for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam.  They include the online publications (lectures and exams), CBA (CERTIFIED BIOMEDICAL AUDITOR) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the CBA (CERTIFIED BIOMEDICAL AUDITOR) prep class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends
				""",

			], None),
		faq('Am I guaranteed to pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?',
			2,
			'Alpha Superior Quality Training has an industry leading pass rate for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam, and an optional first-time passing guarantee for the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  We guarantee you will pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.   ',
			[], None),
		faq('What can you tell me about the CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			4,
			"""
Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge.  
Computer Delivered – The CBA (CERTIFIED BIOMEDICAL AUDITOR) examination is a one-part, 145 – multiple choice question, four-and-a-half-hour exam and is offered in English only.  135 multiple choice questions are scored and 10 are unscored.  
Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 135- multiple choice questions, for your exam and is offered in English only.  
All examinations are open book.  Each participant must bring his or her own reference materials. 

			""",
			[], None),
		faq('How many questions are on the CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			5, """
	Computer Delivered – The CBA (CERTIFIED BIOMEDICAL AUDITOR) examination is a one-part, 135 – multiple choice questions, four-and-a-half-hour exam and is offered in English only.  125 multiple choice questions are scored and 10 are unscored.  
	Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, 125- multiple choice questions, five your exam and is offered in English only.  

					""", [], None),
		faq(
			'How much time do I have to complete the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?    ',
			6, """
					4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
					""", [], None),
		faq('Where can I take the CBA (CERTIFIED BIOMEDICAL AUDITOR) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, """
					I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CBA (CERTIFIED BIOMEDICAL AUDITOR)?',
			10,
			'Apply for your CBA (CERTIFIED BIOMEDICAL AUDITOR) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/biomedical-auditor')),
		faq('How much does the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam cost?  ',
			11, """

					The cost of taking the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
					""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
					$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam.  
					""", [], None),
		faq(
			'Should I start my CBA (CERTIFIED BIOMEDICAL AUDITOR) application before starting my class?',
			13, """
					I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
					""", [], None),
		faq(
			'What does the CBA (CERTIFIED BIOMEDICAL AUDITOR) Body of Knowledge consist of?  ',
			14,
			'To see the CBA (CERTIFIED BIOMEDICAL AUDITOR) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq(
			'What score is needed to pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam? ',
			15,
			"""
The pass percentage varies from one exam to another.  CBA (CERTIFIED BIOMEDICAL AUDITOR) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CBA (CERTIFIED BIOMEDICAL AUDITOR) certification? ',
			16, """
					To maintain the integrity of your Biomedical Auditor certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.   
					""", [], None),
		faq('Why should I become CBA (CERTIFIED BIOMEDICAL AUDITOR) certified?',
			17, """
	Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The CBA (CERTIFIED BIOMEDICAL AUDITOR) cert is highly respected within industry.  

					""",
			[], None),
		faq(
			'What are the work requirements for the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification?  ',
			18, """

	 Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	The CBA requires five years of work experience including one year in a decision-making position.
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the five-year experience requirement waived, as follows (only one of these waivers may be claimed):
	Associate degree — one year waived
	Bachelor's degree — three years waived
	Master's or doctorate — four years waived
	Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.     


					""", [], None),
		faq(
			'What kind of a calculator can I bring into the CBA (CERTIFIED BIOMEDICAL AUDITOR) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the CBA (CERTIFIED BIOMEDICAL AUDITOR) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

					Upon completion of the computer-based examination, you will receive a printed copy of your CBA (CERTIFIED BIOMEDICAL AUDITOR) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
					""", [], None),
	]

	cqe_faqs = [
		faq('Why take the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam prep course from Alpha Superior Quality Training?',
			1, 'We know you have a choice.  Our goal is to be the most preferred ASQ CQE (CERTIFIED QUALITY ENGINEER) '
			   'exam prep vendor in the world.  Our program is different and better than others and this is why: ',
			[
				'You instructor is friendly, educated, and have real- world quality engineering experience. ',
				'Your instructor has been preparing students for the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam for'
				' over 20 years. ',
				'Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the '
				'place of experience! ',
				'Your instructor has won the prestigious Shingo Award for research and publication.  ',
				'From all this experience at taking ASQ exams we have created specific techniques that are unique '
				'to Alpha Superior Quality Training.  This information is contained in two lectures '
				'entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this '
				'material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this '
				'technique I was able to finish the 5 hour exam in approximately '
				'1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).',
				'All of our lectures in the ASQ CQE (CERTIFIED QUALITY ENGINEER) test prep class are in HD video '
				'and may be watched over your mobile devices. ',
				'Our class is published over an easy to use LMS (Learning Management System) that allows all the '
				'functionality needed for your successful ASQ CQE (CERTIFIED QUALITY ENGINEER) preparation '
				'efforts.',
				'The CQE (CERTIFIED QUALITY ENGINEER) prep class consists of 103 lectures that covers the ASQ '
				'CQE ( '
				'CERTIFIED QUALITY ENGINEER) BOK in an easy to understand and enjoyable format. ',
				'Our CQE (CERTIFIED QUALITY ENGINEER) prep class uses approximately 1,000 test question '
				'associated '
				'with the lectures and over another 1,000 questions in our practice exams. ',
				'10. Part of our ASQ CQE (CERTIFIED QUALITY ENGINEER) test prep uses the Indiana Quality '
				'Councils CQE '
				'(CERTIFIED QUALITY ENGINEER) primer.  After every chapter you read there are test questions that '
				'cover your reading assignment.  You instructor gives a lecture over half the questions, '
				'to explain '
				'the logic used in deriving the correct answer for the various questions, the other half is left '
				'for '
				'you to practice with.  On the mathematical chapters your instructor will work all problems at '
				'the end '
				'of those chapters.  You will listen to the solutions, turn off the video, and then work them on '
				'your '
				'own.  This makes it less painful to develop the skill you need to pass the ASQ CQE (CERTIFIED '
				'QUALITY '
				'ENGINEER) certification exam. ',
				'After you finish all of the online training and the associated exams and finish reading the '
				'Primer we then have you take our practice exams.  We have 12 exams available but most students '
				'will '
				'take only three.  Once you experience an 80% rate, or above, on your practice exams you are '
				'ready for '
				'the ASQ CQE (CERTIFIED QUALITY ENGINEER) certification.  This is what we have learned from 20 '
				'years '
				'of experience in preparing students for the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam. ',
				'Our first time pass rate is over 90% compared to the general populations pass rate of '
				'approximately 58%.  The students that fail the exam, that have taken our class, are usually the '
				'ones '
				'that don’t finish all of the class requirements. ',
				'We have the best guarantee in the industry.  '
				'We guarantee you will pass the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam on the first try.  If '
				'you '
				'don’t pass on the first attempt we will pay for your retake.  If you don’t pass it on the second '
				'attempt we will pay for your third retake. If you fail the third time we will give you the rest '
				'of '
				'your money back minus the amount we paid for your retakes.  To qualify for this guarantee you '
				'must '
				'complete all class requirements.  To see class requirements press here:  You must listen to all '
				'lectures and take associated exams for those lectures.  You must finish all exams at a 75% or '
				'above. '
				'This shouldn’t be a problem as you are allowed to take practice exams before you take the real '
				'exam. '
				'You must also read the Primer and finish the questions at the end of each chapter, remember I '
				'go over '
				'half of these with you.  You must finish at least 4 practice exams and them take a series of '
				'final '
				'exams and finish these at over an 80%.  Finishing these requirements will qualify you for the '
				'guarantee. ',
				'We present the CQE (CERTIFIED QUALITY ENGINEER) material in a natural format, similar to how you '
				'work through a quality engineering project.  Other providers just simply review the BOK in an '
				'effort '
				'to get you through the exam.  Our objective is twofold, one objective is to get you to pass the '
				'ASQ '
				'CQE (CERTIFIED QUALITY ENGINEER) exam on the first try and the second objective is to educate '
				'you in '
				'such a way as to make you a powerful quality engineer that will allow you to advance your '
				'career. ',
				'We use three publications to help you prepare for the ASQ CQE (CERTIFIED QUALITY ENGINEER) '
				'certification exam.  They include the online publications (lectures and exams), CQE '
				'(CERTIFIED QUALITY ENGINEER) Primer, Instructor Notes.  The Instructor Notes consists of hard '
				'copies '
				'of the online lectures.  The Instructor Notes have an appendix that have all applicable '
				'formulas in '
				'one place so you can look them up quickly and confidently.  We also have the complete BOK, '
				'at least the statistics elements, in flow charts.  Not all students use this as it can be '
				'somewhat '
				'overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  '
				'These '
				'flowcharts contain all of the formulas with the assumptions and sometimes even example problems.',
				'You will be assigned a time where you can check into a live tutorial session. ',
				'You will have one year access to your online account.  Obviously, most students don''t '
				'need the whole year.  Most students will finish the ASQ CQE (CERTIFIED QUALITY ENGINEER) prep '
				'class '
				'in approximately 3+ months.  Most of our students are full time employees while preparing for '
				'the '
				'exam so the 3+ months is studying during evenings and weekends. '
			], None),
		faq('Am I guaranteed to pass the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CQE (CERTIFIED QUALITY '
			   'ENGINEER) certification exam, and an optional first-time passing guarantee for the ASQ CQE (CERTIFIED '
			   'QUALITY ENGINEER) exam.  This means that if you purchase the guarantee, we guarantee you will pass '
			   'the test on your first try.  The ASQ CQE (CERTIFIED QUALITY ENGINEER) exam is a difficult test and we '
			   'take our guarantee seriously. If you don’t pass on the first attempt we will pay for your retake.  If '
			   'you don’t pass it on the second attempt we will pay for your third retake. If you fail the third time '
			   'we will give you the rest of your money back minus the amount we paid for your retakes.  To qualify '
			   'for this guarantee you must complete all class requirements.  <b>To see class requirements press '
			   'here</b>: '
			   'You must listen to all lectures and take associated exams for those lectures.  You must finish all '
			   'exams at a 75% or above.  This shouldn’t be a problem as you are allowed to take practice exams '
			   'before you take the real exam.  You must also read the Primer and finish the questions at the end of '
			   'each chapter, remember I go over half of these with you.  You must finish at least 4 practice exams '
			   'and them take a series of final exams and finish these at over an 80%.  Finishing these requirements '
			   'will qualify you for the guarantee.  ', [], None),
		faq('What is your pass rate for the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our '
			   'plan, over 95% will pass the certification exam on their first attempt.  In summary what we have '
			   'found is those students who follow our plan will pass.   ', [], None),
		faq('What can you tell me about the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			4,
			'Each certification candidate is required to pass an examination that consists of multiple-choice '
			'questions that measure comprehension of the Body of Knowledge. '
			'Computer Delivered – The CQE (CERTIFIED QUALITY ENGINEER) examination is a one-part, 175 – multiple '
			'choice question, five-and-a-half-hour exam and is offered in English only.  160 multiple choice '
			'questions are scored and 15 are unscored. '
			'Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, '
			'160- multiple '
			'choice questions, five your exam and is offered in English only. '
			'All examinations are open book.  Each participant must bring his or her own reference materials. ',
			[], None),
		faq('How many questions are on the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			5, 'Computer Delivered – The CQE (CERTIFIED QUALITY ENGINEER) examination is a one-part, '
			   '175 – multiple choice question, five-and-a-half-hour exam and is offered in English only.  160 '
			   'multiple choice questions are scored and 15 are unscored. '
			   'Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, '
			   '160- multiple choice questions, five your exam and is offered in English only. ', [], None),
		faq('How much time do I have to complete the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			6, '5 ½ hours for the electronic exam and 5 hours for the paper and pencil exam.  Paper and pencil exams '
			   'are usually taken at ASQ events such as their annual symposiums.', [], None),
		faq('Where can I take the ASQ CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more '
			   'expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and '
			   'the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is '
			   'approximately $100.  ', [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq('Where do I start my application for becoming a CQE (CERTIFIED QUALITY ENGINEER)?',
			10, 'Apply for your CQE (CERTIFIED QUALITY ENGINEER) application directly on ASQ’s website here. ', [],
			Link('Application', 'https://asq.org/cert/quality-engineer')),
		faq('How much does the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam cost?',
			11, 'The cost of taking the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam as an ASQ member is $348 and the '
				'cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate '
				'Member) is approximately $100.  ', [], None),
		faq('How much does membership in ASQ cost?',
			12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member '
				'to take the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam.', [], None),
		faq('Should I start my CQE (CERTIFIED QUALITY ENGINEER) application before starting my class?',
			13, 'I would suggest you get started with the application earlier than later.  Students that get '
				'everything planned out and set up tend to be better because they now have deadlines they are working '
				'toward.  For the average working student I would schedule the exam for four months out from the date '
				'you start the class.', [], None),
		faq('What does the CQE (CERTIFIED QUALITY ENGINEER) Body of Knowledge consist of?',
			14, 'To see the CQE (CERTIFIED QUALITY ENGINEER) BOK go here.  ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/docs/2016/CQE (CERTIFIED QUALITY ENGINEER) %20BOK%202015.pdf')),
		faq('What score is needed to pass the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam?',
			15,
			'The pass percentage varies from one exam to another.  CQE (CERTIFIED QUALITY ENGINEER) students all have '
			'the potential to take a different exam from one another.  I always tell my students if you can get an '
			'80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below '
			'80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  '
			'Those that pass the exam are only told they passed but will not be given the percentage they achieved. '
			'Panels of experts examine every certification exam to ensure that the grading process provides an accurate'
			' assessment of a candidate''s proficiency. The experts establish the passing score for an exam and also '
			'use statistical analysis to make sure that different versions of an exam are equally challenging. '
			'ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 '
			'to be '
			'certified.', [], None),
		faq('What is needed to maintain a CQE (CERTIFIED QUALITY ENGINEER) certification?',
			16, 'To maintain the integrity of your Quality Engineer certification, ASQ requires that you recertify '
				'every three years.  You must earn 18 recertification units (RU’s) within a three-year '
				'recertification period or retake the certification exam at the end of the 3 year recertification '
				'period.   ', [], None),
		faq('Why should I become ASQ CQE (CERTIFIED QUALITY ENGINEER) certified?',
			17, 'Quality competition is a fact of life, and the need for a work force proficient in the principles '
				'and practices of quality control is a central concern of many companies. Certification is a mark of '
				'excellence. It demonstrates that the certified individual has the knowledge to assure quality of '
				'products and services. Certification is an investment in your career and in the future of your '
				'employer. '
				'One of my students was recently looking for a new job without success.  This student decided to get '
				'the CQE (CERTIFIED QUALITY ENGINEER) certification and after successfully taking the exam posted it '
				'on his resume and the next day got three calls for interviews and got several offers from those '
				'interviews.  The ASQ CQE (CERTIFIED QUALITY ENGINEER) cert is highly respected within industry.',
			[], None),
		faq('What are the work requirements for the ASQ CQE (CERTIFIED QUALITY ENGINEER) certification?',
			18, 'You must have at least eight years of higher education and / or work experience in one of more of '
				'the areas of the exam Body of Knowledge (BOK), including a minimum of three years in a '
				'decision-making position.  If you have a degree, diploma or certificate program beyond high school, '
				'you may waive some of the required experience.  Deduct 1 year from certificate / diploma from a '
				'technical or trade school.  Deduct 2 years for an Associate’s degree.  Deduct 4 years for a '
				'Bachelor’s degree.  Deduct 5 years for a Master’s / Doctoral degree.  Please contact me if you have '
				'more questions concerning this requirement.', [], None),
		faq('What kind of a calculator can I bring into the ASQ CQE (CERTIFIED QUALITY ENGINEER) exam?',
			19,
			'I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CQE (CERTIFIED '
			'QUALITY ENGINEER) certification exam.  I will also teach you how to use this calculator though out the '
			'instructional material. '
			'All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test '
			'Center '
			'Administrators can provide you with a hand-held basic calculator upon request. '
			'With the introduction of tablets and palmtop computers and the increasing sophistication of scientific '
			'calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted '
			'for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable '
			'memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to '
			'store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button. '
			'Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are '
			'programmable. The examination is written so that a simple calculator will be sufficient to perform all '
			'calculations. Examinees are allowed to have a backup calculator if necessary.', [], None),
		faq('How long does it take to get my exam results?',
			20, 'Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CQE ('
				'CERTIFIED QUALITY ENGINEER) test results. In addition to the overall pass/fail status, '
				'important diagnostic information on your performance is provided to candidates who fail the '
				'examination. This information provides specific guidance to failing candidates.', [], None),
	]

	cqe_faqs2 = [
		faq('Why take the CQE (CERTIFIED QUALITY ENGINEER) exam prep course from Alpha Superior Quality Training?',
			1, 'We know you have a choice.  Our goal is to be the most preferred CQE (CERTIFIED QUALITY ENGINEER) '
			   'exam prep vendor in the world.  Our program is different and better than others and this is why: ',
			[
				'You instructor is friendly, educated, and have real- world quality engineering experience. ',
				'Your instructor has been preparing students for the CQE (CERTIFIED QUALITY ENGINEER) exam for'
				' over 20 years. ',
				'Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the '
				'place of experience! ',
				'Your instructor has won the prestigious Shingo Award for research and publication.  ',
				'From all this experience at taking ASQ exams we have created specific techniques that are unique '
				'to Alpha Superior Quality Training.  This information is contained in two lectures '
				'entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this '
				'material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this '
				'technique I was able to finish the 5 hour exam in approximately '
				'1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).',
				'All of our lectures in the CQE (CERTIFIED QUALITY ENGINEER) test prep class are in HD video '
				'and may be watched over your mobile devices. ',
				'Our class is published over an easy to use LMS (Learning Management System) that allows all the '
				'functionality needed for your successful CQE (CERTIFIED QUALITY ENGINEER) preparation '
				'efforts.',
				'The CQE (CERTIFIED QUALITY ENGINEER) prep class consists of 103 lectures that covers the '
				'CQE ( '
				'CERTIFIED QUALITY ENGINEER) BOK in an easy to understand and enjoyable format. ',
				'Our CQE (CERTIFIED QUALITY ENGINEER) prep class uses approximately 1,000 test question '
				'associated '
				'with the lectures and over another 1,000 questions in our practice exams. ',
				'10. Part of our CQE (CERTIFIED QUALITY ENGINEER) test prep uses the Indiana Quality '
				'Councils CQE '
				'(CERTIFIED QUALITY ENGINEER) primer.  After every chapter you read there are test questions that '
				'cover your reading assignment.  You instructor gives a lecture over half the questions, '
				'to explain '
				'the logic used in deriving the correct answer for the various questions, the other half is left '
				'for '
				'you to practice with.  On the mathematical chapters your instructor will work all problems at '
				'the end '
				'of those chapters.  You will listen to the solutions, turn off the video, and then work them on '
				'your '
				'own.  This makes it less painful to develop the skill you need to pass the CQE (CERTIFIED '
				'QUALITY '
				'ENGINEER) certification exam. ',
				'After you finish all of the online training and the associated exams and finish reading the '
				'Primer we then have you take our practice exams.  We have 12 exams available but most students '
				'will '
				'take only three.  Once you experience an 80% rate, or above, on your practice exams you are '
				'ready for '
				'the CQE (CERTIFIED QUALITY ENGINEER) certification.  This is what we have learned from 20 '
				'years '
				'of experience in preparing students for the CQE (CERTIFIED QUALITY ENGINEER) exam. ',
				'Our first time pass rate is over 90% compared to the general populations pass rate of '
				'approximately 58%.  The students that fail the exam, that have taken our class, are usually the '
				'ones '
				'that don’t finish all of the class requirements. ',
				'We have the best guarantee in the industry.  '
				'We guarantee you will pass the CQE (CERTIFIED QUALITY ENGINEER) exam on the first try.  If '
				'you '
				'don’t pass on the first attempt we will pay for your retake.  If you don’t pass it on the second '
				'attempt we will pay for your third retake. If you fail the third time we will give you the rest '
				'of '
				'your money back minus the amount we paid for your retakes.  To qualify for this guarantee you '
				'must '
				'complete all class requirements.  To see class requirements press here:  You must listen to all '
				'lectures and take associated exams for those lectures.  You must finish all exams at a 75% or '
				'above. '
				'This shouldn’t be a problem as you are allowed to take practice exams before you take the real '
				'exam. '
				'You must also read the Primer and finish the questions at the end of each chapter, remember I '
				'go over '
				'half of these with you.  You must finish at least 4 practice exams and them take a series of '
				'final '
				'exams and finish these at over an 80%.  Finishing these requirements will qualify you for the '
				'guarantee. ',
				'We present the CQE (CERTIFIED QUALITY ENGINEER) material in a natural format, similar to how you '
				'work through a quality engineering project.  Other providers just simply review the BOK in an '
				'effort '
				'to get you through the exam.  Our objective is twofold, one objective is to get you to pass the '
				''
				'CQE (CERTIFIED QUALITY ENGINEER) exam on the first try and the second objective is to educate '
				'you in '
				'such a way as to make you a powerful quality engineer that will allow you to advance your '
				'career. ',
				'We use three publications to help you prepare for the CQE (CERTIFIED QUALITY ENGINEER) '
				'certification exam.  They include the online publications (lectures and exams), CQE '
				'(CERTIFIED QUALITY ENGINEER) Primer, Instructor Notes.  The Instructor Notes consists of hard '
				'copies '
				'of the online lectures.  The Instructor Notes have an appendix that have all applicable '
				'formulas in '
				'one place so you can look them up quickly and confidently.  We also have the complete BOK, '
				'at least the statistics elements, in flow charts.  Not all students use this as it can be '
				'somewhat '
				'overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  '
				'These '
				'flowcharts contain all of the formulas with the assumptions and sometimes even example problems.',
				'You will be assigned a time where you can check into a live tutorial session. ',
				'You will have one year access to your online account.  Obviously, most students don''t '
				'need the whole year.  Most students will finish the CQE (CERTIFIED QUALITY ENGINEER) prep '
				'class '
				'in approximately 3+ months.  Most of our students are full time employees while preparing for '
				'the '
				'exam so the 3+ months is studying during evenings and weekends. '
			], None),
		faq('Am I guaranteed to pass the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			2, 'Alpha Superior Quality Training has an industry leading pass rate for the CQE (CERTIFIED QUALITY '
			   'ENGINEER) certification exam, and an optional first-time passing guarantee for the CQE (CERTIFIED '
			   'QUALITY ENGINEER) exam.  This means that if you purchase the guarantee, we guarantee you will pass '
			   'the test on your first try.  The CQE (CERTIFIED QUALITY ENGINEER) exam is a difficult test and we '
			   'take our guarantee seriously. If you don’t pass on the first attempt we will pay for your retake.  If '
			   'you don’t pass it on the second attempt we will pay for your third retake. If you fail the third time '
			   'we will give you the rest of your money back minus the amount we paid for your retakes.  To qualify '
			   'for this guarantee you must complete all class requirements.  <b>To see class requirements press '
			   'here</b>: '
			   'You must listen to all lectures and take associated exams for those lectures.  You must finish all '
			   'exams at a 75% or above.  This shouldn’t be a problem as you are allowed to take practice exams '
			   'before you take the real exam.  You must also read the Primer and finish the questions at the end of '
			   'each chapter, remember I go over half of these with you.  You must finish at least 4 practice exams '
			   'and them take a series of final exams and finish these at over an 80%.  Finishing these requirements '
			   'will qualify you for the guarantee.  ', [], None),
		faq('What is your pass rate for the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our '
			   'plan, over 95% will pass the certification exam on their first attempt.  In summary what we have '
			   'found is those students who follow our plan will pass.   ', [], None),
		faq('What can you tell me about the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			4,
			'Each certification candidate is required to pass an examination that consists of multiple-choice '
			'questions that measure comprehension of the Body of Knowledge. '
			'Computer Delivered – The CQE (CERTIFIED QUALITY ENGINEER) examination is a one-part, 175 – multiple '
			'choice question, five-and-a-half-hour exam and is offered in English only.  160 multiple choice '
			'questions are scored and 15 are unscored. '
			'Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, '
			'160- multiple '
			'choice questions, five your exam and is offered in English only. '
			'All examinations are open book.  Each participant must bring his or her own reference materials. ',
			[], None),
		faq('How many questions are on the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			5, 'Computer Delivered – The CQE (CERTIFIED QUALITY ENGINEER) examination is a one-part, '
			   '175 – multiple choice question, five-and-a-half-hour exam and is offered in English only.  160 '
			   'multiple choice questions are scored and 15 are unscored. '
			   'Paper and pencil exams are offered at some ASQ events (symposiums) and consists of one part, '
			   '160- multiple choice questions, five your exam and is offered in English only. ', [], None),
		faq('How much time do I have to complete the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			6, '5 ½ hours for the electronic exam and 5 hours for the paper and pencil exam.  Paper and pencil exams '
			   'are usually taken at ASQ events such as their annual symposiums.', [], None),
		faq('Where can I take the CQE (CERTIFIED QUALITY ENGINEER) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8, 'I would recommend it.  You can take the exam without becoming a member of ASQ however it is more '
			   'expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and '
			   'the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is '
			   'approximately $100.  ', [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq('Where do I start my application for becoming a CQE (CERTIFIED QUALITY ENGINEER)?',
			10, 'Apply for your CQE (CERTIFIED QUALITY ENGINEER) application directly on ASQ’s website here. ', [],
			Link('Application', 'https://asq.org/cert/quality-engineer')),
		faq('How much does the CQE (CERTIFIED QUALITY ENGINEER) exam cost?',
			11, 'The cost of taking the CQE (CERTIFIED QUALITY ENGINEER) exam as an ASQ member is $348 and the '
				'cost as a non-member is $498 (save $150 if you are a member). The cost of joining ASQ (Associate '
				'Member) is approximately $100.  ', [], None),
		faq('How much does membership in ASQ cost?',
			12, '$159 for full membership and $99 for associate membership.  You only need to be an associate member '
				'to take the CQE (CERTIFIED QUALITY ENGINEER) exam.', [], None),
		faq('Should I start my CQE (CERTIFIED QUALITY ENGINEER) application before starting my class?',
			13, 'I would suggest you get started with the application earlier than later.  Students that get '
				'everything planned out and set up tend to be better because they now have deadlines they are working '
				'toward.  For the average working student I would schedule the exam for four months out from the date '
				'you start the class.', [], None),
		faq('What does the CQE (CERTIFIED QUALITY ENGINEER) Body of Knowledge consist of?',
			14, 'To see the CQE (CERTIFIED QUALITY ENGINEER) BOK go here.  ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/docs/2016/CQE (CERTIFIED QUALITY ENGINEER) %20BOK%202015.pdf')),
		faq('What score is needed to pass the CQE (CERTIFIED QUALITY ENGINEER) exam?',
			15,
			'The pass percentage varies from one exam to another.  CQE (CERTIFIED QUALITY ENGINEER) students all have '
			'the potential to take a different exam from one another.  I always tell my students if you can get an '
			'80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below '
			'80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  '
			'Those that pass the exam are only told they passed but will not be given the percentage they achieved. '
			'Panels of experts examine every certification exam to ensure that the grading process provides an accurate'
			' assessment of a candidate''s proficiency. The experts establish the passing score for an exam and also '
			'use statistical analysis to make sure that different versions of an exam are equally challenging. '
			'ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 '
			'to be '
			'certified.', [], None),
		faq('What is needed to maintain a CQE (CERTIFIED QUALITY ENGINEER) certification?',
			16, 'To maintain the integrity of your Quality Engineer certification, ASQ requires that you recertify '
				'every three years.  You must earn 18 recertification units (RU’s) within a three-year '
				'recertification period or retake the certification exam at the end of the 3 year recertification '
				'period.   ', [], None),
		faq('Why should I become CQE (CERTIFIED QUALITY ENGINEER) certified?',
			17, 'Quality competition is a fact of life, and the need for a work force proficient in the principles '
				'and practices of quality control is a central concern of many companies. Certification is a mark of '
				'excellence. It demonstrates that the certified individual has the knowledge to assure quality of '
				'products and services. Certification is an investment in your career and in the future of your '
				'employer. '
				'One of my students was recently looking for a new job without success.  This student decided to get '
				'the CQE (CERTIFIED QUALITY ENGINEER) certification and after successfully taking the exam posted it '
				'on his resume and the next day got three calls for interviews and got several offers from those '
				'interviews.  The CQE (CERTIFIED QUALITY ENGINEER) cert is highly respected within industry.',
			[], None),
		faq('What are the work requirements for the CQE (CERTIFIED QUALITY ENGINEER) certification?',
			18, 'You must have at least eight years of higher education and / or work experience in one of more of '
				'the areas of the exam Body of Knowledge (BOK), including a minimum of three years in a '
				'decision-making position.  If you have a degree, diploma or certificate program beyond high school, '
				'you may waive some of the required experience.  Deduct 1 year from certificate / diploma from a '
				'technical or trade school.  Deduct 2 years for an Associate’s degree.  Deduct 4 years for a '
				'Bachelor’s degree.  Deduct 5 years for a Master’s / Doctoral degree.  Please contact me if you have '
				'more questions concerning this requirement.', [], None),
		faq('What kind of a calculator can I bring into the CQE (CERTIFIED QUALITY ENGINEER) exam?',
			19,
			'I recommend the TI-30XA.  This calculator will offer everything you need to pass the CQE (CERTIFIED '
			'QUALITY ENGINEER) certification exam.  I will also teach you how to use this calculator though out the '
			'instructional material. '
			'All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test '
			'Center '
			'Administrators can provide you with a hand-held basic calculator upon request. '
			'With the introduction of tablets and palmtop computers and the increasing sophistication of scientific '
			'calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted '
			'for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable '
			'memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to '
			'store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button. '
			'Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are '
			'programmable. The examination is written so that a simple calculator will be sufficient to perform all '
			'calculations. Examinees are allowed to have a backup calculator if necessary.', [], None),
		faq('How long does it take to get my exam results?',
			20, 'Upon completion of the computer-based examination, you will receive a printed copy of your CQE ('
				'CERTIFIED QUALITY ENGINEER) test results. In addition to the overall pass/fail status, '
				'important diagnostic information on your performance is provided to candidates who fail the '
				'examination. This information provides specific guidance to failing candidates.', [], None),
	]



	cmq_faqs = [
		faq('Why take the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam preparation course from Alpha Superior Quality Training?',
			1, """
			We know you have a choice.  Our goal is to be the most preferred ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam preparation vendor in the world.  Our program is different and better than others and this is why:
				""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation efforts.  
				""",
				"""
				The CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) prep class consists of 111 lectures that covers the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test prep uses the Indiana Quality Councils CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification.  This is what we have learned from 20 years of experience in preparing students for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We present the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) material in a natural format, similar to how you work through a Manager of Quality / Operational Excellence project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Manager of Quality / Operational Excellence  that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam.  They include the online publications (lectures and exams), CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?',
			2, 'Alpha Superior Quality Training has an industry leading pass rate for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam, and an optional first-time passing guarantee for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  We guarantee you will pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ', [], None),
		faq('What is your pass rate for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			3, 'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.    ', [], None),
		faq('What can you tell me about the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			5, """
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""", [], None),
		faq('How much time do I have to complete the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?  ',
			6, """
			4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
			""", [], None),
		faq('Where can I take the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ASQ?',
			8,"""
			I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
			""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq('Where do I start my application for becoming a CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE)?',
			10, 'Apply for your CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) application directly on ASQ’s website here.   ', [],
			Link('Application', 'https://asq.org/cert/manager-of-quality')),
		faq('How much does the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam cost?  ',
			11, """
			
			The cost of taking the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
			""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
			$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  
			""", [], None),
		faq('Should I start my CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) application before starting my class?',
			13, """
			I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
			""", [], None),
		faq('What does the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Body of Knowledge consist of?  ',
			14, 'To see the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq('What score is needed to pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam? ',
			15,
			"""
			The pass percentage varies from one exam to another.  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq('What is needed to maintain a CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification? ',
			16, """
			To maintain the integrity of your Manager of Quality / Operational Excellence certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.   
			""", [], None),
		faq('Why should I become ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certified?',
			17, """
			Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
One of my students was recently looking for a new job without success.  This student decided to get the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) cert is highly respected within industry.  

			""",
			[], None),
		faq('What are the work requirements for the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification?  ',
			18, """
			
			Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
Candidates must have 10 years of on-the-job experience in one or more of the areas of the Certified Manager of Quality/Organizational Excellence Body of Knowledge. A minimum of five years of this experience must be in a decision-making position, defined as the authority to define, execute or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
If you've been certified by ASQ as a quality auditor, reliability engineer, software quality engineer, or quality engineer, experience used to qualify for certification in these fields applies to certification as a manager of quality/organizational excellence, as long as the 10-year minimum requirement is met.
Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the 10-year experience requirement waived, as follows (only one of these waivers may be claimed):
Diploma from a technical or trade school — one year will be waived
Associate degree — two years waived
Bachelor's degree — four years waived
Master's or doctorate — five years waived
Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.

			""", [], None),
		faq('What kind of a calculator can I bring into the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.

			
			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """
			
			Upon completion of the computer-based examination, you will receive a printed copy of your ASQ CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
			""", [], None),
	]

	cmq_faqs2 = [
		faq(
			'Why take the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam preparation course from Alpha Superior Quality Training?',
			1, """
				We know you have a choice.  Our goal is to be the most preferred  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam preparation vendor in the world.  Our program is different and better than others and this is why:
					""",
			[
				"""
				You instructor is friendly, educated, and has real- world Manager of Quality / Operational Excellence experience.
				""",
				"""
				Your instructor has been preparing students for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam for over 20 years.
				""",
				"""
				Your instructor has passed and gotten certified on 16 of the 19 ASQ exams. Nothing can take the place of experience!
				""",
				"""
				Your instructor has won the prestigious Shingo Award for research and publication.  
				""",
				"""
				From all this experience at taking ASQ exams we have created specific techniques that are unique to Alpha Superior Quality Training.  This information is contained in two lectures entitled “Test Taking Strategies”.  We will make you a test taking pro!  Before I created this material it took 5 hours for me to finish a 5 hour ASQ exam, however, after I discovered this technique I was able to finish the 5 hour exam in approximately 1 ½ hours and I aced it.  My record was finishing an ASQ exam in 30 minutes (and yes I aced it!).  
				""",
				"""
				All of our lectures in the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test prep class are in HD video and may be watched over your mobile devices.
				""",
				"""
				Our class is published over an easy to use LMS (Learning Management System) that allows all the functionality needed for your successful  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation efforts.  
				""",
				"""
				The CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) prep class consists of 111 lectures that covers the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) BOK in an easy to understand and enjoyable format.  
				""",
				"""
				Our CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation class uses approximately 1,100 test question associated with the lectures and practice exams.  
				""",
				"""
				Part of our  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test prep uses the Indiana Quality Councils CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) primer.  After every chapter you read there are test questions that cover your reading assignment.  You instructor gives a lecture over half the questions, to explain the logic used in deriving the correct answer for the various questions, the other half is left for you to practice with.  On the mathematical chapters your instructor will work all problems at the end of those chapters.  You will listen to the solutions, turn off the video, and then work them on your own.  This makes it less painful to develop the skill you need to pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam. 
				""",
				"""
				After you finish all of the online training and the associated exams and finish reading the Primer we then have you take our practice exams.  We have 2 exams available.  Once you experience an 80% rate, or above, on your practice exams you are ready for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification.  This is what we have learned from 20 years of experience in preparing students for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  
				""",
				"""
				Our first time pass rate is over 90% compared to the general populations pass rate of approximately 75%.  The students that fail the exam, that have taken our class, are usually the ones that don’t finish all of the class requirements.
				""",
				"""
				We have the best guarantee in the industry.  We guarantee you will pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above.   
				""",
				"""
				We present the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) material in a natural format, similar to how you work through a Manager of Quality / Operational Excellence project.  Other providers just simply review the BOK in an effort to get you through the exam.  Our objective is twofold, one objective is to get you to pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE ) exam on the first try and the second objective is to educate you in such a way as to make you a powerful Manager of Quality / Operational Excellence  that will allow you to advance your career.  
				""",
				"""
				We use three publications to help you prepare for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam.  They include the online publications (lectures and exams), CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Primer, Instructor Notes.  The Instructor Notes consists of hard copies of the online lectures.  The Instructor Notes have an appendix that have all applicable formulas in one place so you can look them up quickly and confidently.  We also have the complete BOK, at least the statistics elements, in flow charts.  Not all students use this as it can be somewhat overwhelming to some, however, if you like flow charts (memory maps) these will be invaluable.  These flowcharts contain all of the formulas with the assumptions and sometimes even example problems. 
				""",
				"""
				You will can sign up for live video conferencing tutorials on an as needed basis without any additional cost.  
				""",
				"""
				You will have one year access to your online account.  Obviously, most students don’t need the whole year.  Most students will finish the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) preparation class in approximately 3+ months.  Most of our students are full time employees while preparing for the exam so the 3+ months is studying during evenings and weekends.    
				""",

			], None),
		faq('Am I guaranteed to pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?',
			2,
			'Alpha Superior Quality Training has an industry leading pass rate for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam, and an optional first-time passing guarantee for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  We guarantee you will pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam on the first try.  If you do not pass on the first try we will pay ½ the price for your first re-take. To qualify for the guarantee you must finish all elements of class requirements which include finishing all online exams with a 75% success rate or higher.  Read the Primer and work through all end of chapter questions.  Take all practice exams and final exams and consistently score 80% or above on these before taking the ASQ certification exam.   ',
			[], None),
		faq('What is your pass rate for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			3,
			'Pass rates have varied over time, we are continuously improving.  Of those attendees who follow our plan, over 95% will pass the certification exam on their first attempt.  In summary what we have found is those students who follow our plan will pass.    ',
			[], None),
		faq('What can you tell me about the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			4,
			"""
			Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

			""",
			[], None),
		faq('How many questions are on the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			5, """
				Each certification candidate is required to pass an examination that consists of multiple-choice questions that measure comprehension of the Body of Knowledge as well as two constructed response (essay) questions.
	Computer Delivered - The CMQ/OE examination is a two-part, 165- multiple choice question, four-and-a-half-hour exam and is offered in English only. 150 questions are scored and 15 are unscored. There are two constructed response (essay) questions.
	Paper and Pencil - The CMQ/OE examination is a two-part, 150- multiple choice question and two constructed response (essay) questions, four-hour exam and is offered in English only.
	All examinations are open book with the exception of the constructed response (essay) portion of the CMQ/OE exam. Each participant must bring his or her own reference materials.

				""", [], None),
		faq(
			'How much time do I have to complete the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?  ',
			6, """
				4 ½ hours for the electronic exam and 4 hours for the paper and pencil exam.  Paper and pencil exams are usually taken at ASQ events such as their annual symposiums.  
				""", [], None),
		faq('Where can I take the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Exam?',
			7, 'The electronic exams are offered at PROMETRIC facilities located around the world.  To find a '
			   'PROMETRIC site near you go to:', [],
			Link('PROMETRIC', 'https://securereg3.prometric.com/Dispatch.aspx')),
		faq('Should I become a member of ?',
			8, """
				I would recommend it.  You can take the exam without becoming a member of ASQ however it is more expensive to take the exam as a non-member.  The cost of taking the exam as an ASQ member is $348 and the cost as a non-member is $498 (save $150). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('Where do I start my application for membership in ASQ?',
			9, 'Apply for membership in ASQ here.  ', [],
			Link('ASQ Membership', 'https://asq.org/membership/become-a-member')),
		faq(
			'Where do I start my application for becoming a CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE)?',
			10,
			'Apply for your CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) application directly on ASQ’s website here.   ',
			[],
			Link('Application', 'https://asq.org/cert/manager-of-quality')),
		faq('How much does the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam cost?  ',
			11, """

				The cost of taking the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam as an ASQ member is $418 and the cost as a non-member is $568 (save $150 if you are a member). The cost of joining ASQ (Associate Member) is approximately $100.  
				""", [], None),
		faq('How much does membership in ASQ cost?',
			12, """
				$159 for full membership and $99 for associate membership.  You only need to be an associate member to take the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam.  
				""", [], None),
		faq(
			'Should I start my CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) application before starting my class?',
			13, """
				I would suggest you get started with the application earlier than later.  Students that get everything planned out and set up tend to be better because they now have deadlines they are working toward.  For the average working student I would schedule the exam for four months out from the date you start the class.  
				""", [], None),
		faq(
			'What does the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) Body of Knowledge consist of?  ',
			14,
			'To see the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) BOK go to the home page and choose the BOK option.   ',
			[], Link('Body of Knowledge',
					 'https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf')),
		faq(
			'What score is needed to pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam? ',
			15,
			"""
			The pass percentage varies from one exam to another.  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) students all have the potential to take a different exam from one another.  I always tell my students if you can get an 80% you will pass, I have always found this to be true.  In reality the percentage is somewhere below 80% but fairly close to that standard.  ASQ will only show you the test results if you fail the exam.  Those that pass the exam are only told they passed but will not be given the percentage they achieved. 
Panels of experts examine every certification exam to ensure that the grading process provides an accurate assessment of a candidate's proficiency. The experts establish the passing score for an exam and also use statistical analysis to make sure that different versions of an exam are equally challenging.
 ASQ certification exams use a "cut-score" process. You need a score of 550 points out of a possible 750 to be certified.

			""", [], None),
		faq(
			'What is needed to maintain a CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification? ',
			16, """
				To maintain the integrity of your Manager of Quality / Operational Excellence certification, ASQ requires that you recertify every three years.  You must earn 18 recertification units (RU’s) within a three-year recertification period or retake the certification exam at the end of the 3 year recertification period.   
				""", [], None),
		faq('Why should I become  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certified?',
			17, """
				Quality competition is a fact of life, and the need for a work force proficient in the principles and practices of quality control is a central concern of many companies. Certification is a mark of excellence. It demonstrates that the certified individual has the knowledge to assure quality of products and services. Certification is an investment in your career and in the future of your employer.
	One of my students was recently looking for a new job without success.  This student decided to get the CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification and after successfully taking the exam posted it on his resume and the next day got three calls for interviews and got several offers from those interviews.  The  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) cert is highly respected within industry.  

				""",
			[], None),
		faq(
			'What are the work requirements for the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification?  ',
			18, """

				Candidates must have worked in a full-time, paid role. Paid intern, co-op or any other course work cannot be applied toward the work experience requirement.
	Candidates must have 10 years of on-the-job experience in one or more of the areas of the Certified Manager of Quality/Organizational Excellence Body of Knowledge. A minimum of five years of this experience must be in a decision-making position, defined as the authority to define, execute or control projects/processes and to be responsible for the outcome. This may or may not include management or supervisory positions.
	If you've been certified by ASQ as a quality auditor, reliability engineer, software quality engineer, or quality engineer, experience used to qualify for certification in these fields applies to certification as a manager of quality/organizational excellence, as long as the 10-year minimum requirement is met.
	Candidates who have completed a degree from a college, university or technical school with accreditation accepted by ASQ will have part of the 10-year experience requirement waived, as follows (only one of these waivers may be claimed):
	Diploma from a technical or trade school — one year will be waived
	Associate degree — two years waived
	Bachelor's degree — four years waived
	Master's or doctorate — five years waived
	Degrees or diplomas from educational institutions outside the United States must be equivalent to degrees from U.S. educational institutions.

				""", [], None),
		faq(
			'What kind of a calculator can I bring into the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) exam?',
			19,
			"""
			I recommend the TI-30XA.  This calculator will offer everything you need to pass the  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) certification exam.  I will also teach you how to use this calculator though out the instructional material.  
All computer-based exams feature a basic scientific calculator on-screen in the exam. Prometric Test Center Administrators can provide you with a hand-held basic calculator upon request. 
With the introduction of tablets and palmtop computers and the increasing sophistication of scientific calculators, ASQ has become increasingly aware of the need to limit the types of calculators permitted for use during the examinations. Any silent, hand-held, battery-operated calculator with no programmable memory will be permitted. Programmable calculators tend to have graphing capabilities, the ability to store text/alphanumeric data by the input of the user, and a function (fn, F1, F2, etc.) button.
Calculators such as the Texas Instruments TI-89 or similar are absolutely not allowed because they are programmable. The examination is written so that a simple calculator will be sufficient to perform all calculations. Examinees are allowed to have a backup calculator if necessary.


			""", [], None),
		faq('How long does it take to get my exam results?  ',
			20, """

				Upon completion of the computer-based examination, you will receive a printed copy of your  CMQ/OE (CERTIFIED MANAGER OF QUALITY / OPERATIONAL EXCELLENCE) test results. In addition to the overall pass/fail status, important diagnostic information on your performance is provided to candidates who fail the examination. This information provides specific guidance to failing candidates.
				""", [], None),
	]

	title = collections.OrderedDict()
	title["SITE"] = "unknown"
	title["SHORT"] = "()()()"
	title["SHORTCODE"] = "()"
	title["VIDEOURL"] = ""
	title["LECTURE"] = "assets/images/l.pdf"
	title["BOK"] = "https://asq.org"

	faq_1 = None
	faq_2 = None

	html = 'index.html'

	if "cmq" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Manager of Quality"
		title["SHORT"] = "ASQ CMQ/OE"
		title["SHORTCODE"] = "CMQ"
		title["VIDEOURL"] = "mp4:SD/49/M-4388_Why+choose+us+(CCT)"
		title["LECTURE"] = "assets/images/lectures_cmq.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/2014-CMQ-OE-BOK.pdf"
		faq_1 = cmq_faqs
		faq_2 = cmq_faqs2

	if "cqi" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Inspector"
		title["SHORT"] = "ASQ CQI"
		title["SHORTCODE"] = "CQI"
		title["VIDEOURL"] = "mp4:49/M-4391_Why+choose+us+(CQI)"
		title["LECTURE"] = "assets/images/lectures_cqi.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/certification/new-cqi-bok-2012.pdf"
		faq_1 = cqi_faqs
		faq_2 = cqi_faqs2

	if "cba" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Biomedical Auditor"
		title["SHORT"] = "ASQ CBA"
		title["SHORTCODE"] = "CBA"
		title["VIDEOURL"] = "mp4:SD/49/M-4385_Why+choose+us+(CBA)"
		title["LECTURE"] = "assets/images/lectures_cba.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2013/2013%20CBA%20Final%20BOK.pdf"
		faq_1 = cba_faqs
		faq_2 = cba_faqs2

	if "cqpa" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Process Analyst"
		title["SHORT"] = "ASQ CQPA"
		title["SHORTCODE"] = "CQPA"
		title["VIDEOURL"] = "mp4:SD/49/M-4393_Why+choose+us+(CQPA)"
		title["LECTURE"] = "assets/images/lectures_cqpa.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/certification/2013%20FINAL-CQPA%20BOK.pdf"
		faq_1 = cqpa_faqs
		faq_2 = cqpa_faqs2

	if "cqa" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Auditor"
		title["SHORT"] = "ASQ CQA"
		title["SHORTCODE"] = "CQA"
		title["VIDEOURL"] = "mp4:49/M-4389_Why+choose+us+(CQA)"
		title["LECTURE"] = "assets/images/lectures_cqa.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2012%20CQA%20Final%20BOK.pdf"
		faq_1 = cqa_faqs
		faq_2 = cqa_faqs2

	if "cqe" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Engineer"
		title["SHORT"] = "ASQ CQE"
		title["SHORTCODE"] = "CQE"
		title["VIDEOURL"] = "mp4:49/M-4390_Why+choose+us+(CQE)"
		title["LECTURE"] = "assets/images/lectures_cqe.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2016/CQE%20BOK%202015.pdf"
		faq_1 = cqe_faqs
		faq_2 = cqe_faqs2

	if "ccqm" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Construction Quality Manager"
		title["SHORT"] = "ASQ CCQM"
		title["SHORTCODE"] = "C"
		title["VIDEOURL"] = "mp4:SD/49/M-4386_Why+choose+us+(CCQM)"
		title["LECTURE"] = "assets/images/lectures_ccqm.pdf"
		title["BOK"] = "https://asq.org/"
		faq_1 = ccqm_faqs
		faq_2 = ccqm_faqs2

	if "cct" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Calibration Technician"
		title["SHORT"] = "ASQ CCT"
		title["SHORTCODE"] = "CCT"
		title["VIDEOURL"] = "mp4:SD/49/M-4387_Why+choose+us+(CCT)"
		title["LECTURE"] = "assets/images/lectures_cct.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/cct_bok.pdf"
		faq_1 = cct_faqs
		faq_2 = cct_faqs2

	if "cqia" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Improvement Associate"
		title["SHORT"] = "ASQ CQIA"
		title["SHORTCODE"] = "CQIA"
		title["VIDEOURL"] = "mp4:SD/49/M-4392_Why+choose+us+(CQIA)"
		title["LECTURE"] = "assets/images/lectures_cqia.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/2014-CQIA-BOK.pdf"
		faq_1 = cqia_faqs
		faq_2 = cqia_faqs2

	if "cqt" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Technician"
		title["SHORT"] = "ASQ CQT"
		title["SHORTCODE"] = "CQT"
		title["VIDEOURL"] = "mp4:SD/49/M-4394_Why+choose+us+(CQT)"
		title["LECTURE"] = "assets/images/lectures_cqt.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/certification/2012%20CQT%20BOK.pdf"
		faq_1 = cqt_faqs
		faq_2 = cqt_faqs2

	if "cre" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Reliability Engineer"
		title["SHORT"] = "ASQ-CRE"
		title["SHORTCODE"] = "CRE"
		title["VIDEOURL"] = "mp4:SD/1/Why+choose+us+(CRE)" #LOOK AT WITH JOHN
		title["LECTURE"] = "assets/images/lectures_cre.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/cre_bok.pdf"
		faq_1 = cre_faqs
		faq_2 = cre_faqs2

	if "csqp" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Supplier Quality Professional"
		title["SHORT"] = "ASQ CSQP"
		title["SHORTCODE"] = "CSQP"
		title["VIDEOURL"] = "mp4:SD/49/M-4396_Why+choose+us+(CSQP)"
		title["LECTURE"] = "assets/images/lectures_csqp.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2016/CSQP%202016%20Final%20BOK.pdf"
		faq_1 = csqp_faqs
		faq_2 = csqp_faqs2

	if "cssbb" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Six Sigma Black Belt"
		title["SHORT"] = "ASQ CSSBB"
		title["SHORTCODE"] = "CSSBB"
		title["VIDEOURL"] = "mp4:SD/49/M-4397_Why+choose+us+(CSSBB)"
		title["LECTURE"] = "assets/images/lectures_cssbb.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2015/2015%20CSSBB%20BOK.pdf"
		faq_1 = cssbb_faqs
		faq_2 = cssbb_faqs2

	if "cssgb" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Six Sigma Green Belt"
		title["SHORT"] = "ASQ CSSGB"
		title["SHORTCODE"] = "CSSGB"
		title["VIDEOURL"] = "mp4:SD/49/M-4398_Why+choose+us+(CSSGB)"
		title["LECTURE"] = "assets/images/lectures_cssgb.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2014/FINAL%202014%20Green%20Belt%20BOK.pdf"
		faq_1 = csgb_faqs
		faq_2 = csgb_faqs2

	if "cssyb" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Six Sigma Yellow Belt"
		title["SHORT"] = "ASQ CSSYB"
		title["SHORTCODE"] = "CSSYB"
		title["VIDEOURL"] = "mp4:SD/49/M-4399_Why+choose+us+(CSSYB)"
		title["LECTURE"] = "assets/images/lectures_cssyb.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/certification/SSYB%20BOK-final.pdf"
		faq_1 = cssyb_faqs
		faq_2 = cssyb_faqs2

	#incomplete
	if "gdt" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Quality Engineer"
		title["SHORT"] = "ASQ CQE"
		title["SHORTCODE"] = "CQE"
		title["VIDEOURL"] = "mp4:49/M-4390_Why+choose+us+(CQE)"
		title["LECTURE"] = "assets/images/lectures_cqe.pdf"
		title["BOK"] = "https://asq.org/cert/resource/docs/2016/CQE%20BOK%202015.pdf"
		faq_1 = None
		faq_2 = None

	if "gsip" in request.META["HTTP_HOST"].lower():
		title["SITE"] = "Certified Six Sigma Yellow Belt"
		title["SHORT"] = "ASQ CSSYB"
		title["SHORTCODE"] = "CSSYB"
		title["VIDEOURL"] = "mp4:SD/49/M-4399_Why+choose+us+(CSSYB)"
		title["LECTURE"] = "assets/images/lectures_cssyb.pdf"
		title["BOK"] = "https://asq.org/cert/resource/pdf/certification/SSYB%20BOK-final.pdf"
		faq_1 = cssyb_faqs
		faq_2 = cssyb_faqs2

		return render(request, 'gsip.html',
					  {"date": date, "form2": LoginHeaderForm(), "faqs": faq_1, "faqs2": faq_2, "title": title})



	title["SITE"] = "Certified Six Sigma Black Belt"
	title["SHORT"] = "ASQ CSSBB"
	title["SHORTCODE"] = "CSSBB"
	title["VIDEOURL"] = "mp4:SD/49/M-4397_Why+choose+us+(CSSBB)"
	title["LECTURE"] = "assets/images/lectures_cssbb.pdf"
	title["BOK"] = "https://asq.org/cert/resource/docs/2015/2015%20CSSBB%20BOK.pdf"
	faq_1 = cssbb_faqs
	faq_2 = cssbb_faqs2
	return render(request, 'index.html', {"date": date, "form2": LoginHeaderForm(), "faqs": faq_1, "faqs2": faq_2, "title": title})

#
# def db(request):
# 	greeting = Greeting()
# 	greeting.save()
#
# 	greetings = Greeting.objects.all()
#
# 	return render(request, 'db.html', {'greetings': greetings, "form2": LoginHeaderForm()})


# def ajax(request):
# 	form = PostForm()
# 	return render(request, 'ajax.html', {"form": form})


# def create_post(request):
# 	if request.method == 'POST':
# 		post_text = request.POST.get('the_post')
# 		response_data = {}
#
# 		post = Post(text=post_text, author='Kelly')
# 		# post.save()
#
# 		response_data['key'] = "KEY"
# 		response_data['value'] = post.author
#
# 		return HttpResponse(
# 			json.dumps(response_data),
# 			content_type="application/json",
# 		)
# 	else:
# 		return HttpResponse(
# 			json.dumps({"nothing to see": "this isn't happening"}),
# 			content_type="application/json"
# 		)


def api(request):
	return render(request, 'token.html')


def api_login(request):
	username = 'k'
	password = 'kelly123'
	user = authenticate(request, username=username, password=password)
	if user is not None:
		login(request, user)
		return HttpResponse("{ \"msg\":\"IN\" }")
	else:
		return HttpResponse("{ \"msg\":\"OUT\" }")


@login_required
def contact_requests(request):
	contact_reqs = ContactUsRequest.objects.all()
	return render(request, 'contact_requests.html', {"contact_reqs": contact_reqs})


def contact(request):
	if request.method == 'POST':

		req = ContactUsRequest()
		req.name = request.POST.get('name')
		req.email = request.POST.get('email')
		req.message = request.POST.get('message')

		req.save()

		return render(request, 'thanks.html', {"req_id": req.id})
	else:
		return render(request, 'contact.html')


def log_them_out(request):
	if request.user.is_authenticated():
		logout(request)

	date = datetime.now().year
	return redirect('/', {"date": date, "form2": LoginHeaderForm()})


def atc(request):
		return render(request, 'atc_main.html')


def gsip(request):
		return render(request, 'gsip.html')



