from django.db import models
from django.contrib import admin


class SiteHTML(models.Model):
    website = models.CharField(max_length=250, null=False,verbose_name="Site")
    html = models.TextField(verbose_name="HTML")
    square_catalog_code = models.TextField()


class SiteHTMLAdmin(admin.ModelAdmin):
    list_display = ['website']

class ContactUsRequest(models.Model):
    name = models.CharField(max_length=300, null=False)
    email = models.EmailField(null=False)
    message = models.CharField(max_length=8000, null=True)

    def __str__(self):
        return self.name + ': ' + self.email


admin.site.register(ContactUsRequest)
admin.site.register(SiteHTML, SiteHTMLAdmin)
