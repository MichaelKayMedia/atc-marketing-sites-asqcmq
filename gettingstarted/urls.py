from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.contrib import admin
# from rest_framework.documentation import include_docs_urls
from django.urls import reverse

import hello.views
from django.http import HttpResponse

admin.autodiscover()

# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'users', hello.views.UserViewSet)
# router.register(r'groups', hello.views.GroupViewSet)
#
# schema_view = get_swagger_view(title='ATC-WebApi')
# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),


# class StaticViewSitemap(sitemaps.Sitemap):
#     priority = 0.5
#     changefreq = 'daily'
#
#     def items(self):
#         return ['main', 'about', 'license']
#
#     def location(self, item):
#         return reverse(item)
#
#
# sitemaplist = {
# 	'static': StaticViewSitemap
# }

urlpatterns = [
	url(r'^$', hello.views.db_source, name='index'),
	url(r'^login/$', auth_views.login),
	url(r'^thanks/$', hello.views.thanks),
	url(r'^paytest/$', hello.views.pay_test),
	url(r'^pay/(?P<site_code>\w{0,50})/$', hello.views.pay_redirect),
	url(r'^test/$', hello.views.test_html),
	url(r'^logout/$', hello.views.log_them_out),
	#url(r'^db', hello.views.db, name='db'),
	url(r'^admin/', include(admin.site.urls)),
	#url(r'^create_post/$', hello.views.create_post),
	#url(r'^test/$', hello.views.ajax),
	url(r'^login/', auth_views.login),
	url(r'^contact/', hello.views.contact),
	url(r'^requests/', hello.views.contact_requests),
	url(r'^atc/', hello.views.atc),
	url(r'^gsip/', hello.views.gsip),
	url(r'^api/', hello.views.api),
	url(r'^api/login', hello.views.api_login),
	url(r'^db/', hello.views.db_source),
	#url(r'^', include(router.urls)),
	#url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	#url(r'^docs/', include_docs_urls(title='My API title')),
	#url(r'^schema/', schema_view),
	#url(r'^sitemap\.xml$', sitemap,  {'sitemaps': sitemaplist},
    #     name='django.contrib.sitemaps.views.sitemap'),
	url(r'^robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow:", content_type="text/plain"), name="robots_file")
]
